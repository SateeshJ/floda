export class Product {
  id: number;
  name: String;
  description: String;
  price: number;
  discount_price: number;
  images: Array<String>;
  size: Array<String>;
  color: Array<String>;
  quantity: number;
  isWishlist: boolean;
  has_options: any;
  has_extras: any;
  product_payable_price: number;
}