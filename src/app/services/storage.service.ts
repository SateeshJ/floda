import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment, httpOptions } from '../../environments/environment';
import { Storage } from '@ionic/storage';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../services/common.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
    private storage: Storage,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private common: CommonService
  ) { }

  getStorage(ITEMS_KEY): Promise<any[]> {
    return this.storage.get(ITEMS_KEY);
  }

  setStorageValueOld(product: any, ITEMS_KEY): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (items) {
        items.push(product);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [product]);
      }
    });
  }




  setStorageValue(product: any, ITEMS_KEY): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (items) {



        let isNew = true;
        let newItems: any[] = [];
        let k = 0;
        for (let i of items['cartItems']) {
          if (i.productDto.productId === product.id) {
            let newQuantity = i.quantity + 1;
            i.quantity = newQuantity;
            isNew = false;
            product.quantity = newQuantity;
            product = product;
          }
          k++;
        }

        if (isNew) {
          items['cartItems'].push(product);
        }

        newItems = items;

        let cartId = "";
        if (localStorage.getItem('cartId')) {
          cartId = localStorage.getItem('cartId');
        }


        let data = {
          'cartId': cartId,
          'productId': product.id,
          'quantity': product.quantity
        };
        this.addToCart(data).subscribe((res: any) => {
          this.spinner.hide();

          this.toastr.success("Added to cart!");
          return this.storage.set(ITEMS_KEY, res);


        },
          (err: any) => {
            this.spinner.hide();
            if (!this.common.checkValidAuthResponseCode(err)) {
              return;
            }

            if (err.error.text) {
              this.toastr.success(err.error.text);
            } else {
              this.toastr.success(err.error.message);
            }


          }
        );



      } else {


        let cartId = "";
        if (localStorage.getItem('cartId')) {
          cartId = localStorage.getItem('cartId');
        }

        let data = {
          'cartId': cartId,
          'productId': product.id,
          'quantity': product.quantity
        };
        this.addToCart(data).subscribe((res: any) => {
          this.spinner.hide();

          localStorage.setItem('cartId', res.cartId),
            this.toastr.success("Added to cart!");
          return this.storage.set(ITEMS_KEY, res);

        },
          (err: any) => {
            this.spinner.hide();
            if (!this.common.checkValidAuthResponseCode(err)) {
              return;
            }

            if (err.error.text) {
              this.toastr.success(err.error.text);
            } else {
              this.toastr.success(err.error.message);
            }


          }
        );




      }
    });
  }

  updateStorageValue(item: any, ITEMS_KEY): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let newItems: any[] = [];

      for (let i of items) {
        if (i.productDto.productId === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }




      let cartId = "";
      if (localStorage.getItem('cartId')) {
        cartId = localStorage.getItem('cartId');
      }

      let data = {
        'cartId': cartId,
        'productId': item.id,
        'quantity': item.quantity
      };
      this.addToCart(data).subscribe((res: any) => {
        this.spinner.hide();
        this.toastr.success("Updated cart!");
        return this.storage.set(ITEMS_KEY, newItems);
      },
        (err: any) => {
          this.spinner.hide();
          if (!this.common.checkValidAuthResponseCode(err)) {
            return;
          }

          if (err.error.text) {
            this.toastr.success(err.error.text);
          } else {
            this.toastr.success(err.error.message);
          }


        }
      );




    });
  }

  removeStorageValueOld(id: number, ITEMS_KEY): Promise<Product> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let toKeep: any[] = [];

      for (let i of items) {
        if (i.id !== id) {
          toKeep.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, toKeep);
    });
  }

  removeStorageValue(product: Product, ITEMS_KEY): Promise<Product> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {



      if (!items || items.length === 0) {
        return null;
      }



      let k = 0;
      for (let i of items) {


        if (i.id === product.id) {

          if (product.hasOwnProperty('has_options') && i.hasOwnProperty('has_options')) {

            if (i.has_options.id === product.has_options.id) {
              items.splice(k, 1);
            }
          }


          if (!product.hasOwnProperty('has_options') && !i.hasOwnProperty('has_options')) {
            items.splice(k, 1);
          }
        }


        k++;
      }
      if (!items || items.length === 0) {
        return this.storage.remove(ITEMS_KEY);
      } else {
        return this.storage.set(ITEMS_KEY, items);
      }
    });
  }


  removeExtraStorageValue(product: Product, extraIndex: any, ITEMS_KEY): Promise<Product> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {



      if (!items || items.length === 0) {
        return null;
      }



      let k = 0;
      for (let i of items) {


        if (i.id === product.id) {



          if (product.hasOwnProperty('has_extras') && i.hasOwnProperty('has_extras')) {
            if (product.hasOwnProperty('has_options') && i.hasOwnProperty('has_options')) {

              if (i.has_options.id === product.has_options.id) {




                let ex = 0;
                for (let j of i.has_extras) {
                  if (ex === extraIndex) {
                    if (items[k].has_extras.splice(ex, 1)) {
                      items[k].product_payable_price = (items[k].product_payable_price - j.price).toFixed(2);
                    }
                  }
                  ex++;
                }





              }


            }


            if (!product.hasOwnProperty('has_options') && !i.hasOwnProperty('has_options')) {

              let ex1 = 0;
              for (let j of i.has_extras) {
                if (ex1 === extraIndex) {
                  if (items[k].has_extras.splice(ex1, 1)) {
                    items[k].product_payable_price = (items[k].product_payable_price - j.price).toFixed(2);
                  }
                }
                ex1++;
              }

            }

            if (items[k].has_extras) {
              if (!items[k].has_extras.length) {
                delete items[k].has_extras;
              }
            }

          }

        }


        k++;
      }
      if (!items || items.length === 0) {
        return this.storage.remove(ITEMS_KEY);
      } else {
        return this.storage.set(ITEMS_KEY, items);
      }
    });
  }


  addToCart(data: any): Observable<any> {

    let requestData = JSON.stringify({
      "cartId": "",
      "discountCode": "",
      "offer": false,
      "productId": data.productId,
      "quantity": data.quantity,
      "storeId": localStorage.getItem('storeId'),
      "token": {
        "fingerprint": {
          "createdAt": 0,
          "deviceFingerprint": localStorage.getItem('deviceFingerPrint'),
          "jsonOtherInfo": "",
          "user_id": 0
        },
        "loginToken": localStorage.getItem('loginToken'),
      }
    }
    );


    const url = environment.rootCloudUrl + "store/addToCart";
    var t = JSON.parse(localStorage.getItem("currentUserProfile"));
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': "Bearer " + t.accessToken })
    };
    return this.http.post(url, requestData, httpOptions)
      .pipe(
        map((response: Response) => response)
      )
  }



  getStorageCOF(ITEMS_KEY): Promise<any[]> {
    return this.storage.get(ITEMS_KEY);
  }

  setStorageValueCOF(product: Product, ITEMS_KEY): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (items) {
        let isNew = true;
        let newItems: any[] = [];
        let k = 0;
        for (let i of items) {
          if (i.id === product.id) {

            if (product.hasOwnProperty('has_options') && i.hasOwnProperty('has_options')) {

              if (i.has_options.id === product.has_options.id) {
                let newQuantity = i.quantity + 1;
                i.quantity = newQuantity;
                i.has_extras = product.has_extras;


                i.product_payable_price = this.common.getProductPayablePrice(i.product_payable_price, i);

                isNew = false;
                product.quantity = newQuantity;
                product.product_payable_price = i.product_payable_price;
                product = product;
              }
            }



            if (!product.hasOwnProperty('has_options') && !i.hasOwnProperty('has_options')) {
              let newQuantity = i.quantity + 1;
              i.quantity = newQuantity;
              i.has_extras = product.has_extras;
              i.product_payable_price = this.common.getProductPayablePrice(i.product_payable_price, i);

              isNew = false;
              product.quantity = newQuantity;
              product.product_payable_price = i.product_payable_price;
              product = product;

            }




          }
          k++;
        }

        if (isNew) {
          product.product_payable_price = this.common.getProductPayablePrice(product.product_payable_price, product);
          items.push(product);
        }
        newItems = items;

        return this.storage.set(ITEMS_KEY, newItems);
      } else {
        product.product_payable_price = this.common.getProductPayablePrice(product.product_payable_price, product);
        return this.storage.set(ITEMS_KEY, [product]);
      }
    });
  }

  updateStorageValueCOF(item: Product, optionId, ITEMS_KEY): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let newItems: any = [];

      for (let i of items) {
        if (i.id === item.id) {
          if (item.hasOwnProperty('has_options') && i.hasOwnProperty('has_options')) {
            if (i.has_options.id === item.has_options.id) {
              newItems.push(item);
            } else {
              newItems.push(i);
            }
          } else if (!item.hasOwnProperty('has_options') && !i.hasOwnProperty('has_options')) {
            newItems.push(item);
          } else {
            newItems.push(i);
          }
        } else {
          newItems.push(i);
        }
      }

      return this.storage.set(ITEMS_KEY, newItems);
    });
  }

  removeStorageValueCOF(id: number, optionId, ITEMS_KEY): Promise<any> {

    return this.storage.get(ITEMS_KEY).then((items: any[]) => {
      if (!items || items.length === 0) {
        return null;
      }

      let toKeep: any = [];

      for (let i of items) {

        if (i.id !== id) {
          toKeep.push(i);
        } else {
          if (optionId !== null) {
            if (i.has_options) {
              if (optionId !== i.has_options.id) {
                toKeep.push(i);
              }
            } else {
              toKeep.push(i);
            }
          } else {
            if (i.has_options) {
              toKeep.push(i);
            }
          }
        }
      }
      return this.storage.set(ITEMS_KEY, toKeep);
    });
  }



  clearStorageValueCOF(ITEMS_KEY): Promise<any> {
    return this.storage.set(ITEMS_KEY, []);
  }


  clearStorageValueALL(): Promise<any> {
    return this.storage.clear();
  }


  setCustomerStorageValueCOF(customerDetails: any, ITEMS_KEY): Promise<any> {
    return this.storage.set(ITEMS_KEY, [customerDetails]);
  }

}
