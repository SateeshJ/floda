import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  environment,
  httpBasicAuthOptions,
  httpOptions,
} from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  constructor(private http: HttpClient) {}
  fetchStoreDetails(data: any): Observable<any> {
    let requestData = JSON.stringify({
      store_id: data.store_name,
    });
    const url = environment.rootCloudUrl + 'getStoreDetails';
    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  getProductById(data: any): Observable<any> {
    let requestData = JSON.stringify({
      id: data.id,
      store_id: data.store_name,
    });
    const url = environment.rootCloudUrl + 'getProductDetailsById';
    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  getReviewById(data: any): Observable<any> {
    let requestData = JSON.stringify({
      id: data.id,
      store_id: data.store_name,
    });
    const url = environment.rootCloudUrl + 'getReviewDetailsById';
    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  placeOrder(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'createOrderWithReceipt';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  gotoEnquiry(data: any): Observable<any> {
    const username = 'xyz';
    const password = 'xyz$tesla!';
    const BasicAuth = 'Basic ' + btoa(username + ':' + password);
    const httpBasicAuthOptions2 = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: BasicAuth,
      }),
    };
    const url = environment.rootCloudUrl + 'createJonasEnquiry';
    return this.http
      .post(url, data, httpBasicAuthOptions2)
      .pipe(map((response: Response) => response));
  }

  placeOrderWithOptionsAndExtras(data: any): Observable<any> {
    const url =
      environment.rootCloudUrl + 'createOrderWithReceiptWithOptionsAndExtras';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }

  bookAnAppointment(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'bookAnAppointment';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }

  validateCouponCode(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'validateCouponCode';

    let requestData = JSON.stringify({
      store_id: data.store_name,
      coupon_code: data.coupon_code,
    });

    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }

  getOrderSummary(data: any): Observable<any> {
    let requestData = JSON.stringify({
      pageNo: data._page,
      pageSize: data._limit,
      sortOption: 'orderCreationDate',
      sortOrder: 'DESC',
      storeId: localStorage.getItem('storeId'),
      token: {
        fingerprint: {
          createdAt: 0,
          deviceFingerprint: localStorage.getItem('deviceFingerPrint'),
          jsonOtherInfo: '',
          user_id: 0,
        },
        loginToken: localStorage.getItem('loginToken'),
      },
    });

    const url = environment.rootCloudUrl + 'store/getOrderSummary';
    var t = JSON.parse(localStorage.getItem('currentUserProfile'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + t.accessToken,
      }),
    };
    return this.http
      .post(url, requestData, httpOptions)
      .pipe(map((response: Response) => response));
  }

  updatePaymentStatus(data: any): Observable<any> {
    const url = environment.rootCloudUrl + 'updatePaymentStatus';
    return this.http
      .post(url, data, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }

  getOrderDetails(data: any): Observable<any> {
    let requestData = JSON.stringify({
      orderId: data.orderId,
      token: {
        fingerprint: {
          createdAt: 0,
          deviceFingerprint: localStorage.getItem('deviceFingerPrint'),
          jsonOtherInfo: '',
          user_id: 0,
        },
        loginToken: localStorage.getItem('loginToken'),
      },
    });

    const url = environment.rootCloudUrl + 'store/getOrderDetails';
    var t = JSON.parse(localStorage.getItem('currentUserProfile'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + t.accessToken,
      }),
    };
    return this.http
      .post(url, requestData, httpOptions)
      .pipe(map((response: Response) => response));
  }
  getAllStoreDetailsListForHomePage(data: any): Observable<any> {
    const username = '$admin.cof$';
    const password = '$admin.cof@123!$';
    const BasicAuth = 'Basic ' + btoa(username + ':' + password);
    const httpBasicAuthOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: BasicAuth,
      }),
    };
    const url = environment.rootCloudUrl + '/getAllStoreDetailsListForHomePage';

    let requestData = JSON.stringify({
      email: data.email,
      secret: data.secret,
    });

    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
  getStoreLagalPagesByEmailID(data: any): Observable<any> {
    let requestData = JSON.stringify({
      store_id: data.store_name,
    });
    const url = environment.rootCloudUrl + 'getStoreLagalPagesByEmailID';
    return this.http
      .post(url, requestData, httpBasicAuthOptions)
      .pipe(map((response: Response) => response));
  }
}
