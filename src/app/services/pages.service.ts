import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor() { }

  getPages() {
    return [
      {
        title: 'Home',
        url: '/tabs/tab1',
        icon: 'home'
      },
      {
        title: 'Categories',
        url: '/tabs/categories',
        icon: 'md-grid'
      },

      {
        title: 'Orders',
        url: '/tabs/orders',
        icon: 'md-checkmark-circle-outline'
      },

    ];
  }
}
