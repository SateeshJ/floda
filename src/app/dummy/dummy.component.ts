import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import * as copy from 'copy-to-clipboard';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from '../services/storage.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.scss'],
})
export class DummyComponent implements OnInit {
  @Input() public cartArr: any[];
  @Input() public customerDetailsArr: any[];
  @Input() public storeDetailsArr: any;
  @Input() public totalAmount: any;
  @Input() public waHref: any;
  @Input() public orderContent: any;
  @Input() public accountEmail: any;
  @Input() public storeName: any;
  @Input() public delivery_charges: any;
  @Input() public delivery_options: any;

  @Input() public bank_options: any;
  @Input() public pickup_day: any;

  @Input() public is_coupon_applied: any;
  @Input() public coupon_discount_value: any;
  @Input() public coupon_details: any;
  @Input() public payable: any;
  @Input() public items: any;

  constructor(
    public modalController: ModalController,
    public storageService: StorageService,
    private toastr: ToastrService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  goBack() {
    this.location.back();
  }
  ngOnInit() {
  }

  generateOrder() {
    window.open(
      'https://api.whatsapp.com/send?phone=' +
        $('#whatsapp').val() +
        '&text=' +
        encodeURIComponent(this.orderContent.toString()),
      '_blank'
    );
  }

  copy_link() {
    copy(this.orderContent);
    this.toastr.success('Order content copied!');
  }

  dismiss() {
    this.modalController.dismiss({
      dismissed: true,
    });
    this.router.navigate([this.storeName]).then((res) => {
      window.location.reload();
    });
  }
}
