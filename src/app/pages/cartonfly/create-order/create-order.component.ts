import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StorageService } from '../../../services/storage.service';
import * as copy from 'copy-to-clipboard';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import {
  environment,
  httpBasicAuthOptions,
} from '../../../../environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss'],
})
export class CreateOrderComponent implements OnInit {
  @Input() public cartArr: any[];
  @Input() public orderId: any;
  @Input() public orderKey: any;
  @Input() public customerDetailsArr: any[];
  @Input() public storeDetailsArr: any;
  @Input() public totalAmount: any;
  @Input() public waHref: any;
  @Input() public orderContent: any;
  @Input() public accountEmail: any;
  @Input() public storeName: any;
  @Input() public delivery_charges: any;
  @Input() public delivery_options: any;

  @Input() public bank_options: any;
  @Input() public pickup_day: any;

  @Input() public is_coupon_applied: any;
  @Input() public coupon_discount_value: any;
  @Input() public coupon_details: any;
  @Input() public payable: any;
  @Input() public items: any;
  payment_return_url: any;
  wipay_payment_gateway_url: any;

  constructor(
    public modalController: ModalController,
    public storageService: StorageService,
    private toastr: ToastrService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.payment_return_url = environment.payment_return_url;
    this.wipay_payment_gateway_url = environment.wipay_payment_gateway_url;
  }

  goBack() {
    this.router.navigate(['/']);
  }

  joinObj(a, attr) {
    var out = [];
    for (var i = 0; i < a.length; i++) {
      out.push(a[i][attr]);
    }
    return out.join(', ');
  }

  getExtraName(item) {
    return item.product_extras[Object.keys(item.product_extras)[0]].name;
  }

  ngOnInit() {}

  generateOrder() {
    window.open(
      'https://api.whatsapp.com/send?phone=' +
        $('#whatsapp').val() +
        '&text=' +
        encodeURIComponent(this.orderContent.toString()),
      '_blank'
    );
  }

  copy_link() {
    copy(this.orderContent);
    this.toastr.success('Order content copied!');
  }

  dismiss() {
    this.modalController.dismiss({
      dismissed: true,
    });
    if (environment.isWhiteLabeled) {
      this.router.navigate(['/']).then((res) => {
        window.location.reload();
      });
    } else {
      this.router.navigate([this.storeName]).then((res) => {
        window.location.reload();
      });
    }
  }
}
