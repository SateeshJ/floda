import { TermsAndConditionsComponent } from './../terms-and-conditions/terms-and-conditions.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ModalController, IonContent } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageService } from '../../../services/storage.service';
import { StoreService } from '../../../services/store.service';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { CreateOrderComponent } from '../../../pages/cartonfly/create-order/create-order.component';
import { BookAppointmentComponent } from '../../../pages/cartonfly/book-appointment/book-appointment.component';

import { ImageSliderComponent } from '../../../pages/cartonfly/image-slider/image-slider.component';

import { ActivatedRoute, Router } from '@angular/router';
import {
  environment,
  httpBasicAuthOptions,
} from '../../../../environments/environment';
import { empty } from 'rxjs';
import { CurrencySymbolPipe } from '../../../currency-symbol.pipe';
import * as introJs from 'intro.js/intro.js';

import { Meta, Title } from '@angular/platform-browser';
import { exit } from 'process';
import { Ng2DeviceService } from 'ng2-device-detector';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  @ViewChild(IonContent, {
    static: false,
  })
  content: IonContent;
  storeName: any;
  href: any;
  orderContent: any;
  storeOpenStatus: any;
  storeData: any = [];
  cartProducts: any = [];
  selectedCategoryProducts: any = [];
  searchSelectedCategoryProducts: any = [];
  searchTerm = '';
  selectedCategory: any = [];
  categoryProducts: any = [];
  specialInstructions: any;
  viewMode: any;
  couponCode: any;
  appliedCouponDetails: any;
  slides: any = [[]];
  introJS = introJs();
  loading = true;
  noProducts = false;

  arrayOne(n: number): any[] {
    return Array(n);
  }
  prodCount: any;
  productLoaded: boolean = false;
  deldate: any;
  onChange($event) {
    $('#pickup_day').val($event.format('dddd, DD MMM, yyyy'));
    this.deldate = $('#pickup_day').val();
  }

  checkInCart(product: any) {
    let flag = false;
    let prodCount = 0;
    if (this.cartProducts && this.cartProducts.length) {
      for (var i = 0; i < this.cartProducts.length; i++) {
        if (product.id === this.cartProducts[i].id) {
          flag = true;
          prodCount = this.cartProducts[i].quantity;
        }
      }
    } else {
      flag = false;
      prodCount = 0;
    }

    if (prodCount) {
      $('.quantityCountProduct' + product.id).html('x' + prodCount);
      $('.quantityCountProduct' + product.id).show();
    } else {
      $('.quantityCountProduct' + product.id).hide();
    }
    this.prodCount = prodCount;
    return prodCount;
  }

  introMethod() {
    this.introJS.setOptions({
      steps: [
        {
          element: '#step1',
          intro:
            'Welcome to our online! <br>Let me guide you on creating order.',
        },
        {
          element: '#step2',
          intro: 'Click here to chat with our store owner.',
          position: 'right',
        },
        {
          element: '#step3',
          intro: 'Choose categories from here.',
          position: 'bottom',
        },
        {
          element: '#step4',
          intro: 'Search for the products from selected categories here.',
          position: 'bottom',
        },
        {
          element: '#step5',
          intro: 'Add/Minus products to the cart from here.',
          position: 'bottom',
        },
        {
          element: '#step6',
          intro: 'Check your cart items here.',
          position: 'bottom',
        },

        {
          element: '#step7a',
          intro: 'Choose your order mode.',
          position: 'bottom',
        },
        {
          element: '#step7b',
          intro: 'Provide your details here.',
          position: 'bottom',
        },
        {
          element: '#step8',
          intro: 'Click here to generate order.',
          position: 'bottom',
        },
      ],
      showProgress: true,
      skipLabel: 'Skip',
      prevLabel: 'Prev',
      nextLabel: 'Next',
      doneLabel: 'Complete',
      overlayOpacity: '0',
    });

    this.introJS.start();
    this.introJS.oncomplete(function () {
      localStorage.setItem('introForStoreActivate', 'true');
    });
  }

  slideOpts = {
    initialSlide: 0,
    loop: true,
    autoplay: true,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };

  checkoutNow() {
    $('#contactNo').focus();
    this.swalToastMessage(
      'Choose your delivery mode & enter required details to create order.',
      'info'
    );
  }

  public submitForm(form: any) {
    if (!form.value) {
      this.swalToastMessage('Invalid Coupon Code!', 'warning');
      return;
    }

    let data = form.value;
    data.store_name = this.storeName;

    this.spinner.show();
    this.store.validateCouponCode(data).subscribe(
      (res: any) => {
        this.spinner.hide();

        if (res.status === 'success') {
          this.appliedCouponDetails = res.message[Object.keys(res.message)[0]];
          this.swalToastMessage('Coupon Applied!', 'success');
        } else {
          this.swalToastMessage(res.message, 'error');
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage('Something went wrong!', 'warning');
      }
    );
  }

  clearCart() {
    swal
      .fire({
        title: 'Are you sure?',
        position: 'top',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!',
      })
      .then((result) => {
        if (result.value) {
          this.storageService
            .clearStorageValueCOF(
              this.storeData.store.name + '-' + 'my-cart-cof'
            )
            .then((res) => {
              swal.fire({
                title: 'Cart Cleared!',
                position: 'top',
                text: 'Your cart is empty.',
                icon: 'success',
              });

              this.getCartItemsCOF();
            });
        }
      });
  }

  clearCustomerDetails() {
    swal
      .fire({
        title: 'Are you sure?',
        position: 'top',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!',
      })
      .then((result) => {
        if (result.value) {
          this.storageService
            .clearStorageValueCOF('my-customer-cof')
            .then((res) => {
              swal.fire({
                title: 'Customer details Cleared!',
                position: 'top',
                text: 'Your customer details is deleted.',
                icon: 'success',
              });

              this.getCustomerDetailsCOF();
            });
        }
      });
  }
  pickupDay = false;
  preference($event) {
    if ($event.target.value) {
      if ($event.target.value === 'd') {
        $('.show-address').show();
        $('.show-pickup-total').hide();
        $('#selectedDelivery').val('d');
        $('#pickup').removeClass('active');
        $('#delivery').addClass('active');
        this.setDeliveryMode('d');
        this.pickupDay = false;
      } else if ($event.target.value === 'p') {
        $('.show-address').hide();
        $('.show-pickup-total').show();
        $('#selectedDelivery').val('p');
        $('#pickup').addClass('active');
        $('#delivery').removeClass('active');
        this.pickupDay = true;
        this.setDeliveryMode('p');
      }
    }
  }

  setDeliveryMode(deliveryMode) {
    if (deliveryMode === 'd') {
      this.swalToastMessage('Ordering mode set to delivery', 'info');
    } else {
      this.swalToastMessage('Ordering mode set to pick up', 'info');
    }
  }

  async openTerms() {
    this.spinner.show();

    const modal = await this.modalController.create({
      component: TermsAndConditionsComponent,
      cssClass: 'terms-and-conditions',
      componentProps: {
        storeName: this.storeName,
      },
    });
    return await modal.present();
  }
  constructor(
    private store: StoreService,
    private modalController: ModalController,
    private spinner: NgxSpinnerService,
    public storageService: StorageService,
    public common: CommonService,
    private ref: ChangeDetectorRef,
    private router: Router,
    public meta: Meta,
    public title: Title,
    private deviceService: Ng2DeviceService,
    public location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.appliedCouponDetails = false;
    this.viewMode = 'grid';
    this.cartProducts = [];
    this.specialInstructions = 'N/A';

    if (environment.isWhiteLabeled) {
      //only for gogroceries
      if (!localStorage.getItem('storeId')) {
        localStorage.setItem('storeId', environment.storeId);
        this.storeName = localStorage.getItem('storeId');
      } else {
        this.storeName = localStorage.getItem('storeId');
      }
      // //only for gogroceries
    } else {
      //only for non-gogroceries
      // this.storeName = this.router.url.split('/')[3];

      /*trying to eliminated # from url*/

      if ((this.storeName = this.router.url.split('/')[1] !== '#')) {
        if ((this.storeName = this.router.url.split('/')[1] === 'store')) {
          this.storeName = this.router.url.split('/')[2];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      } else {
        if ((this.storeName = this.router.url.split('/')[2] === 'store')) {
          this.storeName = this.router.url.split('/')[3];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      }
      /*trying to eliminated # from url*/
    }

    this.getStoreDetails();

    setTimeout(() => {
      // if (this.storeData) {
      //   this.getCartItemsCOF();
      //   this.getCustomerDetailsCOF();
      // }

      $('.container-fluid ul.list-group li:first').trigger('click');
      // this.spinner.hide();

      function swalToastMessage(text, icon) {
        const Toast = swal.mixin({
          toast: true,
          background: '#2f3542',
          position: 'bottom-start',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer);
            toast.addEventListener('mouseleave', swal.resumeTimer);
          },
        });
        Toast.fire({
          icon: icon,
          title: text,
          background: '#2f3542',
        });
      }

      function setDeliveryMode(deliveryMode) {
        if (deliveryMode === 'd') {
          swalToastMessage('Ordering mode set to delivery', 'info');
        } else {
          swalToastMessage('Ordering mode set to pick up', 'info');
        }
      }

      $('#delivery').click(function () {
        $('.show-address').show();
        $('.show-pickup-total').hide();
        $('#selectedDelivery').val('d');
        $('#delivery-mode').val('d');
        $('#pickup').removeClass('active');
        $('#delivery').addClass('active');
        setDeliveryMode('d');
      });

      $('#pickup').click(function () {
        $('.show-address').hide();
        $('.show-pickup-total').show();
        $('#selectedDelivery').val('p');
        $('#delivery-mode').val('p');
        $('#pickup').addClass('active');
        $('#delivery').removeClass('active');
        setDeliveryMode('p');
      });

      if (this.storeData.store) {
        if (this.storeData.store.order_mode) {
          if (
            this.storeData.store.order_mode === 'all' ||
            this.storeData.store.order_mode === 'delivery'
          ) {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        }
      }
    }, 5000);

    $(window).resize(function () {});
  }
  cartForm: FormGroup;
  termsAndConditions: any;
  onTermsChange($event) {
    this.termsAndConditions = $event.target.checked;
  }
  ngOnInit() {}

  ionViewDidEnter() {}

  changeView(view: any) {
    if (view === 'list') {
      this.viewMode = 'list';
    } else {
      this.viewMode = 'grid';
    }
  }

  chat(text = 'Hi!') {
    window.open(
      'https://api.whatsapp.com/send?phone=' +
        $('#whatsapp').val() +
        '&text=' +
        text,
      '_blank'
    );
    return;
  }

  bookAppointment() {
    this.openBookAppointment();
    return;
  }
  async openBookAppointment() {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: BookAppointmentComponent,
      cssClass: 'fullscreen',
      componentProps: {
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        appointmentDetailsArr: this.storeData.appointment_options,
      },
    });

    modal.dismiss();
    return await modal.present();
  }

  setFilteredItems() {
    if (!this.searchTerm) {
      this.searchSelectedCategoryProducts = this.selectedCategoryProducts;
    }
    this.searchSelectedCategoryProducts = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm: any) {
    return this.selectedCategoryProducts.filter((item) => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  chosedDeliveryOption: any;
  deliveryOption($event) {
    this.chosedDeliveryOption =
      this.storeData.delivery_options[$event.target.value];
    this.storeData.store.delivery_charges = this.chosedDeliveryOption
      ? +this.chosedDeliveryOption.price
      : this.normalDeliveryCharges;
  }
  normalDeliveryCharges: any;
  getStoreDetails() {
    this.activatedRoute.params.subscribe((res) => {
      this.storeName = res.storeId2;
      let data = {
        store_name: this.storeName,
      };
      this.spinner.show();
      this.store.fetchStoreDetails(data).subscribe(
        (res: any) => {
          if (!res.message) {
            this.spinner.hide();
            this.storeData = [];
            return true;
          }

          this.productLoaded = true;
          this.storeData = res.message;

          // if (this.storeData) {
          //   this.getCartItemsCOF();
          //   this.getCustomerDetailsCOF();
          // }

          this.normalDeliveryCharges = 0;
          this.storeData.store.delivery_charges = 0;

          this.storeData.delivery_options = res.delivery_options;
          this.storeData.bank_options = res.bank_options;
          this.storeData.pickup_days = res.pickup_days;
          this.storeData.appointment_options = res.appointment_options;

          this.getCartItemsCOF();
          this.getCustomerDetailsCOF();
          //dynamic og tags
          this.meta.updateTag({
            name: 'description',
            content: this.storeData.store.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: this.storeData.store.logo,
          });
          this.title.setTitle(
            this.storeData.store.display_name + ' - Online store.'
          );
          //dynamic og tags

          if (this.storeData.sub_categories) {
            for (var key in this.storeData.sub_categories) {
              if (this.storeData.sub_categories[key].status) {
                if (this.storeData.sub_categories[key].status === 'active') {
                  this.selectedCategory = this.storeData.sub_categories[key];
                  this.categoryProducts = this.storeData.products;
                  this.changeCategory(key);
                  break;
                }
              } else {
                this.selectedCategory = this.storeData.sub_categories[key];
                this.categoryProducts = this.storeData.products;
                this.changeCategory(key);
                break;
              }
            }
          }

          this.checkStoreStatus(
            this.storeData.store.opening_time,
            this.storeData.store.closing_time
          );

          $(document).ready(function () {
            function setDeliveryMode(deliveryMode) {
              if (deliveryMode === 'd') {
                this.swalToastMessage('Ordering mode set to delivery', 'info');
              } else {
                this.swalToastMessage('Ordering mode set to pick up', 'info');
              }
            }

            function notifyMessage(themsg, ntype) {
              $('#notification').finish();
              $('#notification').html(themsg);
              $('#notification').removeClass();
              $('#notification').addClass('notification');
              $('#notification').addClass('is-' + ntype);
              $('#notification').fadeIn().delay(800).fadeOut('slow');
            }

            function swalToastMessage(text, icon) {
              const Toast = swal.mixin({
                toast: true,
                background: '#2f3542',
                position: 'bottom-start',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', swal.stopTimer);
                  toast.addEventListener('mouseleave', swal.resumeTimer);
                },
              });
              Toast.fire({
                icon: icon,
                title: text,
                background: '#2f3542',
              });
            }

            $(document).on('click', '.addToCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass('animate');
              $('.scrollToCartFloatingButtonArea .icon-cart').addClass('shake');

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate'
                );
              }, 1000);

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .icon-cart').removeClass(
                  'shake'
                );
              }, 1000);
            });

            $(document).on('click', '.deductFromCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass(
                'animate-out'
              );

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate-out'
                );
              }, 1000);
            });
            var stickyOffset;
            if ($('.container-fluid ul.list-group').offset()) {
              stickyOffset = $('.container-fluid ul.list-group').offset().top;
            }

            $(window).scroll(function () {
              if ($(window).width() <= 991) {
                var sticky = $('.container-fluid ul.list-group');
                var scroll = $(window).scrollTop();

                if (scroll >= stickyOffset) {
                  sticky.addClass('fixedOnTop');
                } else {
                  sticky.removeClass('fixedOnTop');
                }
              }
            });

            $('.container-fluid ul.list-group li:first').addClass('active');
            $(document).on(
              'click',
              '.container-fluid ul.list-group li',
              function () {
                $('.container-fluid ul.list-group li').removeClass('active');
                $(this).addClass('active');
              }
            );

            $(document).on('keyup', '.product-search', function (event) {
              let search_value = $(this).val();
              var loadUrl =
                $('#baseUrl').val() + $('#subDomain').val() + '/product/search';

              $.get(
                loadUrl,
                {
                  search_value: search_value,
                },
                function (response) {
                  $('#primaryData').html(response);
                }
              );
            });

            $(document).on('click', '.addToCart', function () {
              swalToastMessage(
                $(this).attr('name') + ' added to cart!',
                'success'
              );
            });
          });
          let slideImg = [];
          let slidesKey = Object.keys(this.storeData.slides);
          slidesKey.forEach((element) => {
            slideImg.push(this.storeData.slides[element].image);
          });
          let deviceInfo = this.deviceService.getDeviceInfo();
          if (deviceInfo.os === 'android' || deviceInfo.os === 'mac') {
            this.slides = this.chunk(slideImg, 1);
          } else {
            this.slides = this.chunk(slideImg, 3);
          }
        },
        (err: any) => {
          this.spinner.hide();
          this.storeData = [];
        }
      );
    });
  }

  chunk(arr: any, chunkSize: any) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  checkStoreStatus(start, end) {
    var timeToCheckMilli = new Date().getTime();
    let startTimeMilli = moment(start, 'HH:mm A').valueOf();
    let endTimeMilli = moment(end, 'HH:mm A').valueOf();
    if (
      timeToCheckMilli >= startTimeMilli &&
      timeToCheckMilli <= endTimeMilli
    ) {
      this.storeOpenStatus = 'Open';
    } else {
      this.storeOpenStatus = 'Closed';
    }
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }
  scrollToElement(): void {
    document.getElementById('target').scrollIntoView({ behavior: 'smooth' });
  }
  scrollToElementCD() {
    document.getElementById('cDetails').scrollIntoView({ behavior: 'smooth' });
  }
  toggle = true;
  menuToggle() {
    this.toggle = !this.toggle;
  }
  hambergerToggle = true;
  hambergerMenuToggle() {
    this.hambergerToggle = !this.hambergerToggle;
  }

  changeCategory(index: any) {
    this.hambergerToggle = !this.hambergerToggle;
    if (index === 'reviews') {
      if (this.storeData && this.storeData.reviews) {
        if (this.common.sizeOfObj(this.storeData.reviews)) {
          this.selectedCategory = this.storeData.reviews;
          this.selectedCategoryProducts = [];

          for (let key in this.storeData.reviews) {
            this.selectedCategoryProducts.push(this.storeData.reviews[key]);
          }

          this.setFilteredItems();
        }
      } else {
        this.selectedCategoryProducts = [];
        this.setFilteredItems();
      }

      return;
    } else {
      if (this.storeData && this.storeData.sub_categories) {
        if (this.common.sizeOfObj(this.storeData.sub_categories)) {
          this.selectedCategory = this.storeData.sub_categories[index];
          this.selectedCategoryProducts = [];

          for (let key in this.categoryProducts) {
            if (
              this.selectedCategory.id ===
              this.categoryProducts[key].sub_category_id
            ) {
              this.selectedCategoryProducts.push(this.categoryProducts[key]);
            }
          }

          this.setFilteredItems();
        }
      }
    }
  }

  scrollToLabel(label) {
    var titleELe = document.getElementById(label);
    this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
  }

  total: number = 0;
  payable: number = 0;

  addcartProducts: any = [];

  addToCartCOF(product: any, optionId: any) {
    this.addcartProducts = product;
    this.addcartProducts.images = product.images1;
    this.addcartProducts.quantity = 1;

    if (optionId) {
      this.addcartProducts.has_options.id = optionId;
    }

    // Save cart product in storage
    this.storageService.setStorageValueCOF(
      this.addcartProducts,
      this.storeData.store.name + '-' + 'my-cart-cof'
    );
    this.swalToastMessage(product.name + ' added to cart!', 'success');
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 800);
  }

  joinObj(a, attr) {
    var out = [];
    for (var i = 0; i < a.length; i++) {
      out.push(a[i][attr]);
    }
    return out.join(', ');
  }
  removeFromCart(product: any) {
    swal
      .fire({
        title: 'Are you sure?',
        position: 'top',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!',
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire('Deleted!', 'Item has been deleted.', 'success');
          this.storageService.removeStorageValue(
            product,
            this.storeData.store.name + '-' + 'my-cart-cof'
          );
          setTimeout(() => {
            this.getCartItemsCOF();
          }, 800);
        }
      });
  }
  removeExtraFromCart(product: any, extraIndex: any) {
    this.storageService.removeExtraStorageValue(
      product,
      extraIndex,
      this.storeData.store.name + '-' + 'my-cart-cof'
    );
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 800);
  }

  getExtraName(item) {
    return item.product_extras[Object.keys(item.product_extras)[0]].name;
  }
  getCartItemsCOF() {
    this.storageService
      .getStorageCOF(this.storeData.store.name + '-' + 'my-cart-cof')
      .then((products) => {
        if (products && products.length) {
          this.cartProducts = products;

          let flag = false;
          this.spinner.hide();

          this.total = 0;
          for (var i = 0; i < this.cartProducts.length; i++) {
            if (!this.cartProducts[i].hasOwnProperty('product_payable_price')) {
              flag = true;
            } else {
              if (!this.cartProducts[i].product_payable_price) {
                flag = true;
              }
            }
            this.total += +this.cartProducts[i].product_payable_price;
            this.loading = false;
          }

          if (flag) {
            this.storageService
              .clearStorageValueCOF(
                this.storeData.store.name + '-' + 'my-cart-cof'
              )
              .then((res) => {
                swal.fire({
                  title: 'Cart Outdated!',
                  position: 'top',
                  text: 'Please Shop Again.',
                  icon: 'success',
                });

                this.getCartItemsCOF();
              });
          }
        } else {
          this.cartProducts = [];
          this.loading = false;
          this.noProducts = true;
          this.spinner.hide();
        }
      });
  }
  getCustomerDetailsCOF() {
    this.storageService
      .getStorageCOF('my-customer-cof')
      .then((details) => {
        if (details && details.length) {
          this.addCustomerDetails = details;
          if (this.addCustomerDetails[0].selectedDelivery === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
            this.spinner.hide();
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        } else {
          this.spinner.hide();

          this.addCustomerDetails = [];
        }
      })
      .catch((err) => (this.addCustomerDetails = []));
  }
  minusQuantityCOF(product: any, event: any, optionId: any) {
    if (optionId) {
      product.has_options.id = optionId;
    }

    this.storageService
      .getStorageCOF(this.storeData.store.name + '-' + 'my-cart-cof')
      .then((products) => {
        this.cartProducts = products;
        let items = this.cartProducts;
        if (!items || items.length === 0) {
          this.swalToastMessage(product.name + ' not found in cart!', 'error');
          return null;
        }

        let isNew = true;
        let k = 0;
        for (let i of items) {
          if (i.id === product.id) {
            if (
              product.hasOwnProperty('has_options') &&
              i.hasOwnProperty('has_options')
            ) {
              if (i.has_options.id === product.has_options.id) {
                let newQuantity = i.quantity - 1;

                if (product.hasOwnProperty('wholesale_lot_size')) {
                  if (product.wholesale_lot_size) {
                    newQuantity =
                      i.quantity - +product.wholesale_lot_size <= 0
                        ? 0
                        : i.quantity - +product.wholesale_lot_size;
                  }
                }

                i.quantity = newQuantity;
                isNew = false;
                product.quantity = newQuantity;
                product = product;
              }
            } else if (
              !product.hasOwnProperty('has_options') &&
              !i.hasOwnProperty('has_options')
            ) {
              let newQuantity = i.quantity - 1;

              if (product.hasOwnProperty('wholesale_lot_size')) {
                if (product.wholesale_lot_size) {
                  newQuantity =
                    i.quantity - +product.wholesale_lot_size <= 0
                      ? 0
                      : i.quantity - +product.wholesale_lot_size;
                }
              }

              i.quantity = newQuantity;
              isNew = false;
              product.quantity = newQuantity;
              product = product;
            }
          }
          k++;
        }

        if (!isNew) {
          if (product.quantity >= 1) {
            product.product_payable_price = this.common.getProductPayablePrice(
              product.product_payable_price,
              product
            );

            this.storageService.updateStorageValueCOF(
              product,
              optionId,
              this.storeData.store.name + '-' + 'my-cart-cof'
            );
          } else {
            this.storageService.removeStorageValueCOF(
              product.id,
              optionId,
              this.storeData.store.name + '-' + 'my-cart-cof'
            );
          }
          this.swalToastMessage(
            product.name + ' removed from cart!',
            'success'
          );
        } else {
          this.swalToastMessage(product.name + ' not found in cart!', 'error');
        }

        setTimeout(() => {
          this.getCartItemsCOF();
        }, 800);
      });
  }

  notifyMessage(themsg, ntype) {
    $('#notification').finish();
    $('#notification').html(themsg);
    $('#notification').removeClass();
    $('#notification').addClass('notification');
    $('#notification').addClass('is-' + ntype);
    $('#notification').fadeIn().delay(800).fadeOut('slow');
  }

  swalToastMessage(text, icon, timer = 3000) {
    const Toast = swal.mixin({
      toast: true,
      background: '#2f3542',
      position: 'bottom-start',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      },
    });
    Toast.fire({
      icon: icon,
      title: text,
      background: '#2f3542',
    });
  }

  generateOrder() {
    var mode = $('#selectedDelivery').val();

    if ($('#contactNo').val() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      return false;
    }

    if ($('#name').val() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        return false;
      }
      if ($('#address2').val() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        return false;
      }
    }

    var loadUrl = $('#baseUrl').val() + $('#subDomain').val() + '/cart-order';
    var href = '';
    $.ajax({
      url: loadUrl,
      method: 'GET',
      async: false,
      context: this,
      data: {
        selected_delivery: $('#selectedDelivery').val(),
        name: $('#name').val(),
        contact_country_code: this.countryCode,
        contact_no: $('#contactNo').val(),
        address1: $('#address1').val(),
        address2: $('#address2').val(),
        pincode: '',
        notes: $('#notes').val(),
      },
      success: function (data) {
        data = JSON.parse(data);

        var orderContent = '';
        this.getCartItemsCOF();

        if (this.cartProducts && this.cartProducts.length) {
          var itemsList = '';
          for (var i = 0; i < this.cartProducts.length; i++) {
            let productName = this.cartProducts[i].name;

            if (this.cartProducts[i].hasOwnProperty('has_options')) {
              if (this.cartProducts[i].has_options.value) {
                productName =
                  productName +
                  ' [ ' +
                  this.cartProducts[i].has_options.value +
                  ' ]';
              }
            }

            let extraArr = '';
            if (
              this.cartProducts[i].hasOwnProperty('has_extras') &&
              this.cartProducts[i].hasOwnProperty('product_extras')
            ) {
              if (this.cartProducts[i].has_extras) {
                if (
                  this.cartProducts[i].has_extras.length &&
                  this.cartProducts[i].product_extras
                ) {
                  extraArr =
                    this.getExtraName(this.cartProducts[i]) +
                    ' : ' +
                    this.joinObj(this.cartProducts[i].has_extras, 'value');
                }
              }
            }

            itemsList =
              itemsList +
              '▪️' +
              productName +
              ' x' +
              this.cartProducts[i].quantity +
              ' ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              '' +
              +this.cartProducts[i].product_payable_price +
              '\n' +
              extraArr +
              '\n';
          }

          this.specialInstructions =
            `Special Instructions : ` + $('#notes').val() + `\n------\n`;

          if ($('#selectedDelivery').val() == 'd') {
            orderContent =
              `✅ Delivery Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity  | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details :\nName : ` +
              this.storeData.store.display_name +
              `\nDelivery Option : ` +
              this.delivery_options.name
                ? this.delivery_options.name
                : 'Default' +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nContact : ` +
                  this.countryCode +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
          } else {
            orderContent =
              `✅ Pickup Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details For Pickup:\nName : ` +
              this.storeData.store.display_name +
              `\nContact : ` +
              this.storeData.store.whatsapp_number +
              `\nAddress : ` +
              this.storeData.store.address +
              `\n------\n` +
              this.specialInstructions +
              this.storeData.store.display_name +
              ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details :\nName : ` +
              $('#name').val() +
              `\nContact : ` +
              this.countryCode +
              $('#contactNo').val() +
              `\n------\nHave a nice day, Thank you :)\n`;
          }
        }

        orderContent = encodeURI(orderContent);
        var isSafari =
          navigator.vendor &&
          navigator.vendor.indexOf('Apple') > -1 &&
          navigator.userAgent &&
          navigator.userAgent.indexOf('CriOS') == -1 &&
          navigator.userAgent.indexOf('FxiOS') == -1;
        if (isSafari) {
          href =
            'https://wa.me/' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        } else {
          href =
            'https://api.whatsapp.com/send?phone=' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        }
      },
    }).done(function () {
      window.open(href, '_blank');
    });
  }

  addCustomerDetails: any = [];
  addQuantityCOF(product, index) {
    if (product.quantity) {
      product.quantity = product.quantity + 1;
    } else {
      product.quantity = 1;
      product.quantity = product.quantity + 1;
    }
  }

  countryCode: any;
  telInputObjectWork(e) {
    this.countryCode = '+' + e.dialCode + ' ';
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  errorText: any;

  filteredDeliveryOptions: any;
  goToCreateOrderOptions() {
    this.spinner.show();

    if (this.storeData.store.hasOwnProperty('minimum_order_amount')) {
      if (this.storeData.store.minimum_order_amount) {
        if (!(+this.storeData.store.minimum_order_amount <= this.total)) {
          this.swalToastMessage(
            'MINIMUM ORDER AMOUNT SHOULD BE GREATER THAN ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.storeData.store.minimum_order_amount,
            'warning'
          );
          this.spinner.hide();
          return false;
        }
      }
    }

    var mode = $('#selectedDelivery').val();
    if ($('#delivery-mode').val() === 'none') {
      this.swalToastMessage('Please choose order preference', 'warning');
      this.spinner.hide();
      return false;
    }
    if (this.termsAndConditions === false) {
      this.swalToastMessage('Please Accept Terms & Conditions', 'warning');
      this.spinner.hide();
      return false;
    }
    if ($('#selectedDelivery').val() == 'd') {
      if (this.storeData.delivery_options.length !== 0) {
        if ($('#delivery_option').val() == 'none') {
          this.swalToastMessage('Please choose delivery option', 'warning');
          this.spinner.hide();
          return false;
        }
      }
    }

    if (this.storeData.bank_options.length !== 0) {
      if ($('#bank_option').val() == 'none') {
        this.swalToastMessage(
          'Please choose preferred mode of payment',
          'warning'
        );
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactEmail').val() == '') {
      this.swalToastMessage('Please enter your email address', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test($('#contactEmail').val()) == false) {
        this.swalToastMessage('Please enter valid email address', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactNo').val() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var phoneNum = $('#contactNo').val().replace(/[^\d]/g, '');
      if (phoneNum.length > 6 && phoneNum.length < 12) {
      } else {
        this.swalToastMessage('Please enter valid contact number', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#name').val() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      this.spinner.hide();
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }

      if ($('#address2').val() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if (
      this.storeData.pickup_days.length !== 0 &&
      $('#selectedDelivery').val() == 'p'
    ) {
      if ($('#pickup_day').val() == 'none') {
        this.swalToastMessage('Please choose pickup day', 'warning');
        this.spinner.hide();
        return false;
      }
    }
    let bankOptions = [];

    for (const key in this.storeData.bank_options) {
      if (
        Object.prototype.hasOwnProperty.call(this.storeData.bank_options, key)
      ) {
        const element = this.storeData.bank_options[key];
        bankOptions.push(element);
      }
    }
    const getBankOptions = $('#bank_option').val();
    const filtered = bankOptions.filter((item) => {
      return item.id === getBankOptions;
    });
    let deliveryOptions = [];

    for (const key in this.storeData.delivery_options) {
      if (
        Object.prototype.hasOwnProperty.call(
          this.storeData.delivery_options,
          key
        )
      ) {
        const element = this.storeData.delivery_options[key];
        bankOptions.push(element);
      }
    }
    const getDelivery_options = $('#delivery_option').val();
    this.filteredDeliveryOptions = bankOptions.filter((item) => {
      return item.id === getDelivery_options;
    });
    var custDetails = {
      selectedDelivery: $('#selectedDelivery').val()
        ? $('#selectedDelivery').val()
        : 'none',
      name: $('#name').val() ? $('#name').val() : '',
      contact_country_code: this.countryCode ? this.countryCode : '',
      contactNo: $('#contactNo').val() ? $('#contactNo').val() : '',
      contactEmail: $('#contactEmail').val(),
      address1: $('#address1').val() ? $('#address1').val() : '',
      address2: $('#address2').val() ? $('#address2').val() : '',
      pincode: '',
      notes: $('#notes').val() ? $('#notes').val() : '',
      deliveryMode: $('#selectedDelivery').val()
        ? $('#selectedDelivery').val()
        : '',
      deliveryOption: $('#delivery_option').val()
        ? $('#delivery_option').val()
        : '',
      bankOption: $('#bank_option').val() ? $('#bank_option').val() : '',
    };

    this.storageService.clearStorageValueCOF('my-customer-cof').then((res) => {
      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }
      this.storageService.setCustomerStorageValueCOF(
        custDetails,
        'my-customer-cof'
      );
      this.addCustomerDetails[0] = custDetails;
    });

    let data = JSON.stringify({
      account_id: this.storeData.store.account_email_id,
      store_name: this.storeName,
      email: this.storeData.store.account_email_id,
      selected_delivery: $('#selectedDelivery').val(),
      name: $('#name').val(),
      contact_country_code: this.countryCode,
      contact_no: $('#contactNo').val(),
      contact_email: $('#contactEmail').val(),
      address1: $('#address1').val(),
      address2: $('#address2').val(),
      pincode: '',
      notes: $('#notes').val(),

      currency_symbol: this.storeData.store.currency_symbol,
      total: this.total,
      delivery_charges:
        $('#selectedDelivery').val() === 'd'
          ? this.storeData.store.delivery_charges
          : 0, //new
      delivery_options: this.filteredDeliveryOptions
        ? this.filteredDeliveryOptions[0]
        : 'none',

      bank_options: filtered[0],
      pickup_day:
        $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
          ? $('#pickup_day').val()
          : 'N/A', //new

      is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
      coupon_discount_value: this.appliedCouponDetails
        ? (
            (this.total * this.appliedCouponDetails.discount_percentage) /
            100
          ).toFixed(2)
        : 0, //new
      coupon_details: this.appliedCouponDetails
        ? this.appliedCouponDetails
        : 'false', //new
      payable:
        $('#selectedDelivery').val() === 'd'
          ? (
              this.total +
              (this.storeData.store.delivery_charges
                ? this.storeData.store.delivery_charges
                : 0) -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2)
          : (
              this.total -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2), //new
      items: this.cartProducts,
    });

    this.spinner.show();
    this.store.placeOrderWithOptionsAndExtras(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          this.orderId = res.order_id;
          this.orderKey = res.order_key;
          let data = {
            mode: 'WP',
          };
          if (data.mode === 'WP') {
            var orderContent = '';
            this.getCartItemsCOF();

            if (this.cartProducts && this.cartProducts.length) {
              var itemsList = '';
              for (var i = 0; i < this.cartProducts.length; i++) {
                let productName = this.cartProducts[i].name;

                if (this.cartProducts[i].hasOwnProperty('has_options')) {
                  if (this.cartProducts[i].has_options.value) {
                    productName =
                      productName +
                      ' [ ' +
                      this.cartProducts[i].has_options.value +
                      ' ]';
                  }
                }

                let extraArr = '';
                if (
                  this.cartProducts[i].hasOwnProperty('has_extras') &&
                  this.cartProducts[i].hasOwnProperty('product_extras')
                ) {
                  if (this.cartProducts[i].has_extras) {
                    if (
                      this.cartProducts[i].has_extras.length &&
                      this.cartProducts[i].product_extras
                    ) {
                      extraArr =
                        this.getExtraName(this.cartProducts[i]) +
                        ' : ' +
                        this.joinObj(this.cartProducts[i].has_extras, 'value');
                    }
                  }
                }

                itemsList =
                  itemsList +
                  '▪️' +
                  productName +
                  ' x' +
                  this.cartProducts[i].quantity +
                  ' ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  '' +
                  this.cartProducts[i].product_payable_price +
                  '\n' +
                  extraArr +
                  '\n';
              }

              this.specialInstructions =
                `Special Instructions : ` + $('#notes').val() + `\n------\n`;

              if ($('#selectedDelivery').val() == 'd') {
                orderContent =
                  `✅ Delivery Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nDelivery Charges : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.storeData.store.delivery_charges
                    ? +this.storeData.store.delivery_charges
                    : 0
                  ).toFixed(2) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        (this.storeData.store.delivery_charges
                          ? +this.storeData.store.delivery_charges
                          : 0) -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nDelivery Partner : \n` +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.name
                    : 'Default') +
                  ' ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.price
                    : 0) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nStore Details :\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  this.countryCode +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              } else {
                orderContent =
                  `✅ Pickup Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        this.storeData.store.delivery_charges -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nPreferred pickup day : \n` +
                  ($('#pickup_day').val() && $('#pickup_day').val() !== 'none'
                    ? $('#pickup_day').val()
                    : 'N/A') +
                  `\n------\nStore Details For Pickup:\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  this.countryCode +
                  $('#contactNo').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              }

              this.orderContent = orderContent;

              this.orderContent = this.orderContent.replace(
                '__ORDER__ID__',
                res.order_id
              );
            }

            orderContent = encodeURI(orderContent);
            var isSafari =
              navigator.vendor &&
              navigator.vendor.indexOf('Apple') > -1 &&
              navigator.userAgent &&
              navigator.userAgent.indexOf('CriOS') == -1 &&
              navigator.userAgent.indexOf('FxiOS') == -1;
            if (isSafari) {
              this.href =
                'https://wa.me/' +
                $('#whatsapp').val() +
                '?text=' +
                orderContent.toString();
            } else {
              this.href =
                'https://api.whatsapp.com/send?phone=' +
                $('#whatsapp').val() +
                '&text=' +
                orderContent.toString();
            }
          } else if (data.mode == 'SMS') {
            this.href =
              'sms://' +
              $('#whatsapp').val() +
              ';?&body=' +
              orderContent.toString();
          }

          setTimeout(() => {
            this.getCartItemsCOF();
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
            this.openOrderDetails();
            this.spinner.hide();
          }, 1000);
        } else {
          this.swalToastMessage(
            'Unable to create order, try again later!',
            'warning'
          );
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage(
          'Unable to create order, try again later!',
          'warning'
        );
      }
    );
  }

  orderId: any;
  orderKey: any;
  async openOrderDetails() {
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    let modal = await this.modalController.create({
      component: CreateOrderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        orderId: this.orderId,
        orderKey: this.orderKey,
        cartArr: this.cartProducts,
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        totalAmount: this.total,
        waHref: this.href,
        orderContent: this.orderContent,
        accountEmail: this.storeData.store.account_email_id,
        storeName: this.storeName,
        delivery_charges:
          $('#selectedDelivery').val() === 'd'
            ? this.storeData.store.delivery_charges
            : 0, //new
        delivery_options: this.filteredDeliveryOptions
          ? this.filteredDeliveryOptions[0]
          : 'false', //new
        bank_options: this.storeData.bank_options[$('#bank_option').val()]
          ? this.storeData.bank_options[$('#bank_option').val()]
          : 'N/A', //new
        pickup_day:
          $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
            ? $('#pickup_day').val()
            : 'N/A', //new
        is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
        coupon_discount_value: this.appliedCouponDetails
          ? (
              (this.total * this.appliedCouponDetails.discount_percentage) /
              100
            ).toFixed(2)
          : 0, //new
        coupon_details: this.appliedCouponDetails
          ? this.appliedCouponDetails
          : 'false', //new
        payable:
          $('#selectedDelivery').val() === 'd'
            ? (
                this.total +
                this.storeData.store.delivery_charges -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2)
            : (
                this.total -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2), //new
        items: this.cartProducts,
      },
    });

    this.storageService
      .clearStorageValueCOF(this.storeData.store.name + '-' + 'my-cart-cof')
      .then((res) => {
        this.swalToastMessage(
          'Your order taken successfully, You will receive call from the ' +
            this.storeData.store.display_name +
            ' team shortly.',
          'success',
          6000
        );
        this.getCartItemsCOF();
        this.appliedCouponDetails = false;
        this.couponCode = null;

        if (this.addCustomerDetails) {
          if (this.addCustomerDetails[0].selectedDelivery === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        }

        if (this.storeData.store) {
          if (this.storeData.store.order_mode) {
            if (
              this.storeData.store.order_mode === 'all' ||
              this.storeData.store.order_mode === 'delivery'
            ) {
              $('.show-address').show();
              $('.show-pickup-total').hide();
              $('#selectedDelivery').val('d');
              $('#delivery-mode').val('d');
              $('#pickup').removeClass('active');
              $('#delivery').addClass('active');
            } else {
              $('.show-address').hide();
              $('.show-pickup-total').show();
              $('#selectedDelivery').val('p');
              $('#delivery-mode').val('p');
              $('#pickup').addClass('active');
              $('#delivery').removeClass('active');
            }
          }
        }
      });

    modal.dismiss();
    return await modal.present();
  }

  viewImageSlider(image, additionalImagesArr) {
    if (additionalImagesArr) {
      additionalImagesArr['main'] = { image: image };
    } else {
      additionalImagesArr = { main: { image: image } };
    }

    this.openImageSlider(additionalImagesArr);
    return;
  }
  async openImageSlider(additionalImagesArr) {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: ImageSliderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        additionalImagesArr: additionalImagesArr,
      },
    });

    modal.dismiss();
    return await modal.present();
  }
  goBack() {
    this.location.back();
  }
  goBackHome() {
    this.router.navigate([this.storeName]).then((res) => {
      this.getCartItemsCOF();
    });
  }
  gotoSpeficCategory(itemKey: any) {
    // this.router.navigate(['/']).then((res) => {
    //   this.router.navigate(['/products/' + itemKey]);
    // });
    this.router.navigate(['/products/' + itemKey]);
  }
}
