import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CreateOrderComponent } from '../../../pages/cartonfly/create-order/create-order.component';
import { BookAppointmentComponent } from '../../../pages/cartonfly/book-appointment/book-appointment.component';

import { ImageSliderComponent } from '../../../pages/cartonfly/image-slider/image-slider.component';

import { HomeComponent } from './home.component';
import { HomeTopSliderComponent } from '../../../components/home-top-slider/home-top-slider.component';
import { CurrencySymbolPipe } from '../../../currency-symbol.pipe';
import { NgImageSliderModule } from 'ng-image-slider';
import { Meta } from '@angular/platform-browser';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { CartComponent } from '../cart/cart.component';
import { ViewProductComponent } from '../view-product/view-product.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { LandingComponent } from '../landing/landing.component';
import { PaymentCompletedComponent } from '../payment-completed/payment-completed.component';
import { DummyComponent } from 'src/app/dummy/dummy.component';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { environment } from 'src/environments/environment';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions.component';
import {
  LazyLoadImageModule,
  LAZYLOAD_IMAGE_HOOKS,
  ScrollHooks,
} from 'ng-lazyload-image';
import { OwlModule } from 'ngx-owl-carousel';
import { CalendarModule } from 'ion2-calendar';
import { OurProductsComponent } from './../our-products/our-products.component';
import { ContactUsComponent } from './../contact-us/contact-us.component';
import { TestimonialsComponent } from './../testimonials/testimonials.component';
import { AboutUsComponent } from './../about-us/about-us.component';
import { LightboxModule } from 'ngx-lightbox';

let routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: ':storeId',
    component: HomeComponent,
  },
  {
    path: 'product/:productId',
    component: ViewProductComponent,
  },
  {
    path: 'review/:productId',
    component: ViewProductComponent,
  },
  {
    path: 'cart/:storeId2',
    component: CartComponent,
  },
  {
    path: 'products/:key',
    component: OurProductsComponent,
  },
  {
    path: 'payment-completed/:storeId2',
    children: [
      {
        path: '**',
        component: PaymentCompletedComponent,
      },
    ],
  },
  {
    path: 'dummy/dummy',
    component: DummyComponent,
  },
  {
    path: 'jonadaveselightgiftservices/contact-us',
    component: ContactUsComponent,
  },
  {
    path: 'jonadaveselightgiftservices/testimonials',
    component: TestimonialsComponent,
  },
  {
    path: 'jonadaveselightgiftservices/about-us',
    component: AboutUsComponent,
  },
];
if (environment.isWhiteLabeled) {
  routes = [
    {
      path: '',
      component: HomeComponent,
    },
    {
      path: ':storeId',
      component: HomeComponent,
    },
    {
      path: 'product/:productId',
      component: ViewProductComponent,
    },
    {
      path: 'review/:productId',
      component: ViewProductComponent,
    },
    {
      path: 'cart/:storeId2',
      component: CartComponent,
    },

    {
      path: 'products/:key',
      component: OurProductsComponent,
    },
    {
      path: 'jonadaveselightgiftservices/contact-us',
      component: ContactUsComponent,
    },
    {
      path: 'jonadaveselightgiftservices/testimonials',
      component: TestimonialsComponent,
    },
    {
      path: 'jonadaveselightgiftservices/about-us',
      component: AboutUsComponent,
    },
    {
      path: 'payment-completed/:storeId2',
      children: [
        {
          path: '**',
          component: PaymentCompletedComponent,
        },
      ],
    },
    {
      path: 'dummy/dummy',
      component: DummyComponent,
    },
  ];
}
@NgModule({
  imports: [
    NgImageSliderModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ClipboardModule,
    Ng2TelInputModule,
    RouterModule.forChild(routes),
    Ng2DeviceDetectorModule.forRoot(),
    LazyLoadImageModule,
    OwlModule,
    CalendarModule.forRoot({
      doneLabel: 'Save',
      closeIcon: true,
    }),
    LightboxModule,
  ],
  entryComponents: [
    CreateOrderComponent,
    BookAppointmentComponent,
    ImageSliderComponent,
    TermsAndConditionsComponent,
    OurProductsComponent,
  ],
  exports: [CurrencySymbolPipe],
  declarations: [
    HomeComponent,
    CreateOrderComponent,
    BookAppointmentComponent,
    ImageSliderComponent,
    HomeTopSliderComponent,
    PaymentCompletedComponent,
    CurrencySymbolPipe,
    CartComponent,
    ViewProductComponent,
    LandingComponent,
    DummyComponent,
    TermsAndConditionsComponent,
    OurProductsComponent,
    ContactUsComponent,
    TestimonialsComponent,
    AboutUsComponent,
  ],
  providers: [Meta, { provide: LAZYLOAD_IMAGE_HOOKS, useClass: ScrollHooks }],
})
export class HomeModule {}
