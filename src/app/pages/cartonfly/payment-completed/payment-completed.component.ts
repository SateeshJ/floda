import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StorageService } from '../../../services/storage.service';
import * as copy from 'copy-to-clipboard';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  StoreService
} from '../../../services/store.service';
import {
  NgxSpinnerService
} from 'ngx-spinner';
import {
  environment,
  httpBasicAuthOptions,
} from '../../../../environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './payment-completed.component.html',
  styleUrls: ['./payment-completed.component.scss'],
})
export class PaymentCompletedComponent implements OnInit {
  storeName: any;
  constructor(
    public modalController: ModalController,
    public storageService: StorageService,
    private toastr: ToastrService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,

    private store: StoreService,
    private spinner: NgxSpinnerService,
  ) {

    if (this.router.url.split('/')[2]) {
      this.storeName = this.router.url.split('/')[2].split('?')[0];
    } else {
      this.storeName = null;
    }

  }

  goBack() {
    this.location.back();
  }


  searchParams: any;
  getUrlQueryParams(url: any) {
    var queryString = url.split("?")[1];
    var keyValuePairs = queryString.split("&");
    var keyValue = [];
    var queryParams: any = {};
    keyValuePairs.forEach(function (pair: any) {
      keyValue = pair.split("=");
      queryParams[keyValue[0]] = decodeURIComponent(keyValue[1]).replace(/\+/g, " ");
    });
    return queryParams;
  }
  ngOnInit() {

    this.searchParams = this.getUrlQueryParams(this.router.url);
    this.updatePaymentStatus();


  }


  updatePaymentStatus() {
    if (!this.searchParams || this.searchParams === '') {
      return;
    }

    const data = {
      store_name: this.storeName,
      pay_online_response: this.searchParams
    };

    this.spinner.show();
    this.store.updatePaymentStatus(data).subscribe(
      (res: any) => {
        // this.toastr.success('Payment process completed!');
        this.spinner.hide();
      },
      (err: any) => {
        this.spinner.hide();
      }
    );
  }

}
