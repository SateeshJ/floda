import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';

import * as $ from 'jquery';
@Component({
  selector: 'app-cart',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss'],
})
export class ImageSliderComponent implements OnInit {
  imageArrLength = 0;

  imageObject: any = [];
  slideOpts1 = {
    //initialSlide: 0,
    loop: true,
    autoplay: true,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };
  items: any = [];
  imgClicked = false;
  imgVal: any;
  @ViewChild('imgModal', { read: ElementRef, static: false })
  imgModal: ElementRef;
  imgIndex: any;
  nextBtn = true;
  previousBtn = true;

  @Input() public additionalImagesArr: any;
  constructor(public modalController: ModalController) {}

  ngOnInit() {
    for (var key in this.additionalImagesArr) {
      this.imageArrLength++;
      this.imageObject.push({
        image: this.additionalImagesArr[key].image,
        thumbImage: this.additionalImagesArr[key].image,
        // alt: 'alt of image',
        // title: 'title of image'
      });
    }
  }
  openImg(event, item, i) {
    this.imgClicked = true;
    this.imgVal = item.image;
    this.imgIndex = i;
    if (this.imgIndex === 0) {
      this.previousBtn = false;
    } else {
      this.previousBtn = true;
    }
    if (this.imgIndex === this.items.length - 1) {
      this.nextBtn = false;
    }
  }

  nextImg() {
    this.imgIndex += 1;
    const index = this.imgIndex;
    if (index === this.imageObject.length - 1) {
      this.nextBtn = false;
      this.previousBtn = true;
    } else {
      this.nextBtn = true;
    }
    this.imgVal = this.imageObject[index].image;
    this.previousBtn = true;
  }

  previousImg() {
    this.imgIndex -= 1;
    const index = this.imgIndex;
    if (index === 0) {
      this.previousBtn = false;
    } else {
      this.nextBtn = true;
      this.previousBtn = true;
    }
    this.imgVal = this.imageObject[index].image;
  }
  close() {
    this.imgModal.nativeElement.style.display = 'none';
    this.imgClicked = false;
    this.previousBtn = true;
    this.nextBtn = true;
  }
  dismiss() {
    this.modalController.dismiss({
      dismissed: true,
    });
  }
}
