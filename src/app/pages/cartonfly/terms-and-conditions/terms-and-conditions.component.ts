import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss'],
})
export class TermsAndConditionsComponent implements OnInit {
  constructor(
    private store: StoreService,
    private modalController: ModalController,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {}
  termsAndConditions: any;
  terms: any = [];
  termsData: any = [];
  disclaimer: any;
  privacyPolicy: any;
  shippingPaymentInfo: any;
  returnPolicy: any;
  aboutUs: any;
  @Input() storeName;
  @Input() title;
  noData = false;
  ngOnInit() {
    this.getTermsAndCondiions();
  }
  getTermsAndCondiions() {
    let data = {
      store_name: this.storeName,
    };
    this.store.getStoreLagalPagesByEmailID(data).subscribe((res) => {
      this.termsData = res.message;
      this.spinner.hide();
      if (this.termsData !== null) {
        this.terms = res.message[this.title];
        for (const key in this.termsData) {
          if (Object.prototype.hasOwnProperty.call(this.termsData, key)) {
            const element = this.termsData;
            this.termsAndConditions = element.terms_and_conditions.content;
            this.disclaimer = element.disclaimer.content;
            this.privacyPolicy = element.privacy_policy.content;
            this.shippingPaymentInfo =
              element.shipping_and_payment_info.content;
            this.returnPolicy = element.return_policy.content;
            this.aboutUs = element.about_us.content;
            this.noData = false;
          }
        }
      }
    });
  }
  close() {
    this.modalController.dismiss();
  }
}
