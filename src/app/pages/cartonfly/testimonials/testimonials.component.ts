import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  ElementRef,
  HostListener,
} from '@angular/core';
import { ModalController, IonContent, Platform } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageService } from '../../../services/storage.service';
import { StoreService } from '../../../services/store.service';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
// import * as $ from 'jquery';
import swal from 'sweetalert2';
import { CreateOrderComponent } from '../../../pages/cartonfly/create-order/create-order.component';
import { BookAppointmentComponent } from '../../../pages/cartonfly/book-appointment/book-appointment.component';

import { ImageSliderComponent } from '../../../pages/cartonfly/image-slider/image-slider.component';

import { ActivatedRoute, Router } from '@angular/router';
import {
  environment,
  httpBasicAuthOptions,
} from '../../../../environments/environment';
import { empty } from 'rxjs';
import { CurrencySymbolPipe } from '../../../currency-symbol.pipe';
import * as introJs from 'intro.js/intro.js';

import { Meta, Title } from '@angular/platform-browser';
import { exit } from 'process';
import { Ng2DeviceService } from 'ng2-device-detector';
import { HttpHeaders } from '@angular/common/http';
import { KeyValue, Location } from '@angular/common';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions.component';
declare var $: any;

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss'],
})
export class TestimonialsComponent implements OnInit {
  @ViewChild(IonContent, {
    static: false,
  })
  content: IonContent;
  storeName: any;
  href: any;
  orderContent: any;
  storeOpenStatus: any;
  storeData: any = [];
  cartProducts: any = [];
  selectedCategoryProducts: any = [];
  searchSelectedCategoryProducts: any = [];
  searchTerm = '';
  selectedCategory: any = [];
  categoryProducts: any = [];
  specialInstructions: any;
  viewMode: any;
  couponCode: any;
  appliedCouponDetails: any;
  slides: any = [[]];
  introJS = introJs();
  loading = true;
  termsAndConditions: any;
  terms: any = [];
  termsData: any = [];
  disclaimer: any;
  privacyPolicy: any;
  shippingPaymentInfo: any;
  returnPolicy: any;
  aboutUs: any;
  loadingCart = true;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  prodCount: any;
  productLoaded: boolean = false;
  checkInCart(product: any) {
    let flag = false;
    let prodCount = 0;
    if (this.cartProducts && this.cartProducts.length) {
      for (var i = 0; i < this.cartProducts.length; i++) {
        if (product.id === this.cartProducts[i].id) {
          flag = true;
          prodCount = this.cartProducts[i].quantity;
        }
      }
    } else {
      flag = false;
      prodCount = 0;
    }

    if (prodCount) {
      $('.quantityCountProduct' + product.id).html('x' + prodCount);
      $('.quantityCountProduct' + product.id).show();
    } else {
      $('.quantityCountProduct' + product.id).hide();
    }
    this.prodCount = prodCount;
    return prodCount;
  }

  introMethod() {
    this.introJS.setOptions({
      steps: [
        {
          element: '#step1',
          intro:
            'Welcome to our online! <br>Let me guide you on creating order.',
        },
        {
          element: '#step2',
          intro: 'Click here to chat with our store owner.',
          position: 'right',
        },
        {
          element: '#step3',
          intro: 'Choose categories from here.',
          position: 'bottom',
        },
        {
          element: '#step4',
          intro: 'Search for the products from selected categories here.',
          position: 'bottom',
        },
        {
          element: '#step5',
          intro: 'Add/Minus products to the cart from here.',
          position: 'bottom',
        },
        {
          element: '#step6',
          intro: 'Check your cart items here.',
          position: 'bottom',
        },

        {
          element: '#step7a',
          intro: 'Choose your order mode.',
          position: 'bottom',
        },
        {
          element: '#step7b',
          intro: 'Provide your details here.',
          position: 'bottom',
        },
        {
          element: '#step8',
          intro: 'Click here to generate order.',
          position: 'bottom',
        },
      ],
      showProgress: true,
      skipLabel: 'Skip',
      prevLabel: 'Prev',
      nextLabel: 'Next',
      doneLabel: 'Complete',
      overlayOpacity: '0',
    });

    this.introJS.start();
    this.introJS.oncomplete(function () {
      localStorage.setItem('introForStoreActivate', 'true');
    });
  }

  slideOpts = {
    initialSlide: 0,
    loop: true,
    autoplay: true,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };
  goToCategories() {
    document
      .getElementById('categories')
      .scrollIntoView({ behavior: 'smooth' });
  }
  checkoutNow() {
    $('#contactNo').focus();
    this.swalToastMessage(
      'Choose your delivery mode & enter required details to create order.',
      'info'
    );
  }
  getTermsAndCondiions() {
    let data = {
      store_name: '876faces',
    };
    this.store.getStoreLagalPagesByEmailID(data).subscribe((res) => {
      this.termsAndConditions = res.message;
    });
  }
  async openTerms(event) {
    this.spinner.show();
    const modal = await this.modalController.create({
      component: TermsAndConditionsComponent,
      cssClass: 'terms-and-conditions',
      componentProps: {
        storeName: this.storeName,
        title: event,
      },
    });
    return await modal.present();
  }
  public submitForm(form: any) {
    if (!form.value) {
      this.swalToastMessage('Invalid Coupon Code!', 'warning');
      return;
    }

    let data = form.value;
    data.store_name = this.storeName;

    this.spinner.show();
    this.store.validateCouponCode(data).subscribe(
      (res: any) => {
        this.spinner.hide();

        if (res.status === 'success') {
          this.appliedCouponDetails = res.message[Object.keys(res.message)[0]];
          this.swalToastMessage('Coupon Applied!', 'success');
        } else {
          this.swalToastMessage(res.message, 'error');
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage('Something went wrong!', 'warning');
      }
    );
  }

  clearCart() {
    swal
      .fire({
        title: 'Are you sure?',
        position: 'top',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!',
      })
      .then((result) => {
        if (result.value) {
          this.storageService
            .clearStorageValueCOF(
              this.storeData.store.name.trim() + '-' + 'my-cart-cof'
            )
            .then((res) => {
              swal.fire({
                title: 'Cart Cleared!',
                position: 'top',
                text: 'Your cart is empty.',
                icon: 'success',
              });

              this.getCartItemsCOF();
            });
        }
      });
  }

  clearCustomerDetails() {
    swal
      .fire({
        title: 'Are you sure?',
        position: 'top',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!',
      })
      .then((result) => {
        if (result.value) {
          this.storageService
            .clearStorageValueCOF('my-customer-cof')
            .then((res) => {
              swal.fire({
                title: 'Customer details Cleared!',
                position: 'top',
                text: 'Your customer details is deleted.',
                icon: 'success',
              });

              this.getCustomerDetailsCOF();
            });
        }
      });
  }
  preference($event) {
    if ($event.target.value) {
      if ($event.target.value === 'd') {
        $('.show-address').show();
        $('.show-pickup-total').hide();
        $('#selectedDelivery').val('d');
        $('#pickup').removeClass('active');
        $('#delivery').addClass('active');
        this.setDeliveryMode('d');
      } else if ($event.target.value === 'p') {
        $('.show-address').hide();
        $('.show-pickup-total').show();
        $('#selectedDelivery').val('p');
        $('#pickup').addClass('active');
        $('#delivery').removeClass('active');
        this.setDeliveryMode('p');
      }
    }
  }

  setDeliveryMode(deliveryMode) {
    if (deliveryMode === 'd') {
      this.swalToastMessage('Ordering mode set to delivery', 'info');
    } else {
      this.swalToastMessage('Ordering mode set to pick up', 'info');
    }
  }
  title2 = 'OwlCarousel2 in Angular7 with Custom Navigation Arrows';

  carouselOptions = {
    margin: 25,
    nav: true,
    navText: [
      "<div class='nav-btn prev-slide'></div>",
      "<div class='nav-btn next-slide'></div>",
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 3,
        nav: true,
      },
      600: {
        items: 5,
        nav: true,
      },
      1000: {
        items: 8,
        nav: true,
        loop: false,
      },
      1500: {
        items: 12,
        nav: true,
        loop: true,
      },
    },
  };

  images = [
    {
      text: 'Everfresh Flowers',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/1.jpg',
    },
    {
      text: 'Festive Deer',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/2.jpg',
    },
    {
      text: 'Morning Greens',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/3.jpg',
    },
    {
      text: 'Bunch of Love',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/4.jpg',
    },
    {
      text: 'Blue Clear',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/5.jpg',
    },
    {
      text: 'Evening Clouds',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/7.jpg',
    },
    {
      text: 'Fontains in Shadows',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/8.jpg',
    },
    {
      text: 'Kites in the Sky',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/9.jpg',
    },
    {
      text: 'Sun Streak',
      image: 'https://freakyjolly.com/demo/jquery/PreloadJS/images/10.jpg',
    },
  ];
  constructor(
    private store: StoreService,
    private modalController: ModalController,
    private spinner: NgxSpinnerService,
    public storageService: StorageService,
    public common: CommonService,
    private ref: ChangeDetectorRef,
    private router: Router,
    public meta: Meta,
    public title: Title,
    private activatedRoute: ActivatedRoute,
    private deviceService: Ng2DeviceService,
    private location: Location,
    private platform: Platform
  ) {
    $(document).ready(function () {
      $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $('.navbar-collapse').hasClass('show');
        if (_opened === true && !clickover.hasClass('navbar-toggler')) {
          $('.navbar-toggler').click();
        }
      });
    });
    this.activatedRoute.paramMap.subscribe((res) => {
      let sName = res.get('id');
      if (sName === null) {
        this.storeName = environment.storeId;
      } else {
        this.storeName = sName;
      }
    });

    this.appliedCouponDetails = false;
    this.viewMode = 'grid';
    this.spinner.show();
    this.cartProducts = [];
    this.specialInstructions = 'N/A';

    this.getTermsAndCondiions();
    if (environment.isWhiteLabeled) {
      if (!localStorage.getItem('storeId')) {
        localStorage.setItem('storeId', environment.storeId);
        this.storeName = localStorage.getItem('storeId');
      } else {
        this.storeName = localStorage.getItem('storeId');
      }
    } else {
      if ((this.storeName = this.router.url.split('/')[1] !== '#')) {
        if ((this.storeName = this.router.url.split('/')[1] === 'store')) {
          this.storeName = this.router.url.split('/')[2];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      } else {
        if ((this.storeName = this.router.url.split('/')[2] === 'store')) {
          this.storeName = this.router.url.split('/')[3];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      }
    }

    this.getStoreDetails();
    this.getTermsAndCondiions2();

    $('.container-fluid ul.list-group li:first').trigger('click');
    this.spinner.hide();

    function swalToastMessage(text, icon) {
      const Toast = swal.mixin({
        toast: true,
        background: '#2f3542',
        position: 'bottom-start',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer);
          toast.addEventListener('mouseleave', swal.resumeTimer);
        },
      });
      Toast.fire({
        icon: icon,
        title: text,
        background: '#2f3542',
      });
    }

    function setDeliveryMode(deliveryMode) {
      if (deliveryMode === 'd') {
        swalToastMessage('Ordering mode set to delivery', 'info');
      } else {
        swalToastMessage('Ordering mode set to pick up', 'info');
      }
    }

    $('#delivery').click(function () {
      $('.show-address').show();
      $('.show-pickup-total').hide();
      $('#selectedDelivery').val('d');
      $('#delivery-mode').val('d');
      $('#pickup').removeClass('active');
      $('#delivery').addClass('active');
      setDeliveryMode('d');
    });

    $('#pickup').click(function () {
      $('.show-address').hide();
      $('.show-pickup-total').show();
      $('#selectedDelivery').val('p');
      $('#delivery-mode').val('p');
      $('#pickup').addClass('active');
      $('#delivery').removeClass('active');
      setDeliveryMode('p');
    });

    if (this.storeData.store) {
      if (this.storeData.store.order_mode) {
        if (
          this.storeData.store.order_mode === 'all' ||
          this.storeData.store.order_mode === 'delivery'
        ) {
          $('.show-address').show();
          $('.show-pickup-total').hide();
          $('#selectedDelivery').val('d');
          $('#pickup').removeClass('active');
          $('#delivery').addClass('active');
        } else {
          $('.show-address').hide();
          $('.show-pickup-total').show();
          $('#selectedDelivery').val('p');
          $('#pickup').addClass('active');
          $('#delivery').removeClass('active');
        }
      }
    }

    $(window).resize(function () {});
  }
  getTermsAndCondiions2() {
    let data = {
      store_name: this.storeName,
    };
    this.store.getStoreLagalPagesByEmailID(data).subscribe((res) => {
      this.termsData = res.message;
      this.spinner.hide();
      if (this.termsData !== null) {
        for (const key in this.termsData) {
          if (Object.prototype.hasOwnProperty.call(this.termsData, key)) {
            const element = this.termsData;
            this.termsAndConditions = element.terms_and_conditions.content;
            this.disclaimer = element.disclaimer.content;
            this.privacyPolicy = element.privacy_policy.content;
            this.shippingPaymentInfo =
              element.shipping_and_payment_info.content;
            this.returnPolicy = element.return_policy.content;
            this.aboutUs = element.about_us.content;
          }
        }
      }
    });
  }
  ngOnInit() {
    setTimeout(() => {
      'use strict';
      /*------ Sticky menu start ------*/
      var $window = $(window);
      $window.on('scroll', function () {
        var scroll = $window.scrollTop();
        if (scroll < 300) {
          $('.sticky').removeClass('is-sticky');
        } else {
          $('.sticky').addClass('is-sticky');
        }
      });
      /*------ Sticky menu end ------*/

      /*-------- Background Image JS start --------*/
      var bgSelector = $('.bg-img');
      bgSelector.each(function (index, elem) {
        var element = $(elem),
          bgSource = element.data('bg');
        element.css('background-image', 'url(' + bgSource + ')');
      });
      /*-------- Background Image JS end --------*/

      /*------- Image zoom effect start -------*/
      $('.img-zoom').zoom();
      /*------- Image zoom effect end -------*/

      /*------- tooltip active js start -------*/
      $('[data-toggle="tooltip"]').tooltip();
      /*------- tooltip active js start -------*/

      /*------ Hero main slider active start ------*/
      $('.hero-slider-active').slick({
        autoplay: true,
        fade: true,
        speed: 1000,
        dots: false,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              arrows: false,
            },
          },
        ],
      });
      /*------ Hero main slider active end ------*/

      // Hero main slider active js
      $('.hero-slider-active-2').slick({
        autoplay: true,
        arrows: false,
        speed: 1500,
        dots: true,
        centerMode: true,
        centerPadding: '18%',
        slidesToShow: 1,
        responsive: [
          {
            breakpoint: 1599,
            settings: {
              centerPadding: '10%',
            },
          },
          {
            breakpoint: 1499,
            settings: {
              centerMode: false,
            },
          },
          {
            breakpoint: 768,
            settings: {
              centerMode: false,
            },
          },
        ],
      });

      /*------ Top seller carousel active start ------*/
      $('.product-carousel--4').slick({
        autoplay: false,
        fade: false,
        speed: 1000,
        slidesToShow: 4,
        adaptiveHeight: true,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
            },
          },
        ],
      });
      /*------ Top seller carousel active end ------*/

      /*------ daily deals carousel active start ------*/
      $('.product-deal-carousel--2').slick({
        autoplay: false,
        fade: false,
        speed: 1000,
        slidesToShow: 2,
        adaptiveHeight: true,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
            },
          },
        ],
      });
      /*------ daily deals carousel active end ------*/

      /*-------- prodct details slider active start --------*/
      $('.product-large-slider').slick({
        fade: true,
        arrows: false,
        asNavFor: '.pro-nav',
      });

      // product details slider nav active
      $('.pro-nav').slick({
        slidesToShow: 4,
        asNavFor: '.product-large-slider',
        centerMode: true,
        centerPadding: 0,
        focusOnSelect: true,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 3,
              arrows: false,
            },
          },
        ],
      });
      /*-------- prodct details slider active end --------*/

      /*------ Latest blog carousel active start ------*/
      $('.latest-blog-carousel').slick({
        autoplay: false,
        speed: 1000,
        slidesToShow: 3,
        adaptiveHeight: true,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              arrows: false,
            },
          },
        ],
      });
      /*------ Latest blog carousel active end ------*/

      // testimonial cariusel active js
      $('.testimonial-content-carousel').slick({
        arrows: false,
        asNavFor: '.testimonial-thumb-carousel',
      });

      // product details slider nav active
      $('.testimonial-thumb-carousel').slick({
        slidesToShow: 3,
        asNavFor: '.testimonial-content-carousel',
        centerMode: true,
        arrows: false,
        centerPadding: 0,
        focusOnSelect: true,
      });

      /*------ blog slider active start ------*/
      $('.blog-carousel-active').slick({
        autoplay: true,
        speed: 1000,
        slidesToShow: 1,
        prevArrow:
          '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow:
          '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
      });
      /*------ blog slider active end ------*/

      /*------- offcanvas search form active start -------*/
      $('.offcanvas-btn').on('click', function () {
        $('body').addClass('fix');
        $('.offcanvas-search-inner').addClass('show');
      });

      $('.minicart-btn').on('click', function () {
        $('body').addClass('fix');
        $('.minicart-inner').addClass('show');
      });

      $('.offcanvas-close, .minicart-close,.offcanvas-overlay').on(
        'click',
        function () {
          $('body').removeClass('fix');
          $('.offcanvas-search-inner, .minicart-inner').removeClass('show');
        }
      );
      /*------- offcanvas search form active end -------*/

      /*------- nice select active start -------*/
      $('select').niceSelect();
      /*------- nice select active end -------*/

      /*-------- Off Canvas Open close start--------*/
      $('.off-canvas-btn').on('click', function () {
        $('body').addClass('fix');
        $('.off-canvas-wrapper').addClass('open');
      });

      $('.btn-close-off-canvas,.off-canvas-overlay').on('click', function () {
        $('body').removeClass('fix');
        $('.off-canvas-wrapper').removeClass('open');
      });
      /*-------- Off Canvas Open close end--------*/

      // pricing filter
      var rangeSlider = $('.price-range'),
        amount = $('#amount'),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
      rangeSlider.slider({
        range: true,
        min: 0,
        max: 1200,
        values: [100, 1000],
        slide: function (event, ui) {
          amount.val('$' + ui.values[0] + ' - $' + ui.values[1]);
        },
      });
      amount.val(
        ' $' +
          rangeSlider.slider('values', 0) +
          ' - $' +
          rangeSlider.slider('values', 1)
      );

      /*------- product view mode change js start -------*/
      $('.product-view-mode a').on('click', function (e) {
        e.preventDefault();
        var shopProductWrap = $('.shop-product-wrap');
        var viewMode = $(this).data('target');
        $('.product-view-mode a').removeClass('active');
        $(this).addClass('active');
        shopProductWrap.removeClass('grid-view list-view').addClass(viewMode);
      });
      /*------- product view mode change js end -------*/

      /*------- Countdown Activation start -------*/
      $('[data-countdown]').each(function () {
        var $this = $(this),
          finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function (event) {
          $this.html(
            event.strftime(
              '<div class="single-countdown"><span class="single-countdown__time">%D</span><span class="single-countdown__text">Days</span></div><div class="single-countdown"><span class="single-countdown__time">%H</span><span class="single-countdown__text">Hours</span></div><div class="single-countdown"><span class="single-countdown__time">%M</span><span class="single-countdown__text">Mins</span></div><div class="single-countdown"><span class="single-countdown__time">%S</span><span class="single-countdown__text">Secs</span></div>'
            )
          );
        });
      });
      /*------- Countdown Activation end -------*/

      /*--------- quantity change js start ---------*/
      $('.pro-qty').prepend('<span class="dec qtybtn">-</span>');
      $('.pro-qty').append('<span class="inc qtybtn">+</span>');
      $('.qtybtn').on('click', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
          var newVal = parseFloat(oldValue) + 1;
        } else {
          // Don't allow decrementing below zero
          if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
          } else {
            newVal = 0;
          }
        }
        $button.parent().find('input').val(newVal);
      });
      /*--------- quantity change js end ---------*/

      /*-------- scroll to top start --------*/
      $(window).on('scroll', function () {
        if ($(this).scrollTop() > 600) {
          $('.scroll-top').removeClass('not-visible');
        } else {
          $('.scroll-top').addClass('not-visible');
        }
      });
      $('.scroll-top').on('click', function (event) {
        $('html,body').animate(
          {
            scrollTop: 0,
          },
          1000
        );
      });
      /*-------- scroll to top end --------*/

      /*------- offcanvas mobile menu start -------*/
      var $offCanvasNav = $('.mobile-menu'),
        $offCanvasNavSubMenu = $offCanvasNav.find('.dropdown');

      /*Add Toggle Button With Off Canvas Sub Menu*/
      $offCanvasNavSubMenu
        .parent()
        .prepend('<span class="menu-expand"><i></i></span>');

      /*Close Off Canvas Sub Menu*/
      $offCanvasNavSubMenu.slideUp();

      /*Category Sub Menu Toggle*/
      $offCanvasNav.on('click', 'li a, li .menu-expand', function (e) {
        var $this = $(this);
        if (
          $this
            .parent()
            .attr('class')
            .match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/) &&
          ($this.attr('href') === '#' || $this.hasClass('menu-expand'))
        ) {
          e.preventDefault();
          if ($this.siblings('ul:visible').length) {
            $this.parent('li').removeClass('active');
            $this.siblings('ul').slideUp();
          } else {
            $this.parent('li').addClass('active');
            $this
              .closest('li')
              .siblings('li')
              .removeClass('active')
              .find('li')
              .removeClass('active');
            $this.closest('li').siblings('li').find('ul:visible').slideUp();
            $this.siblings('ul').slideDown();
          }
        }
      });
      /*------- offcanvas mobile menu end -------*/

      /*------- Checkout Page accordion start -------*/
      $('#create_pwd').on('change', function () {
        $('.account-create').slideToggle('100');
      });

      $('#ship_to_different').on('change', function () {
        $('.ship-to-different').slideToggle('100');
      });

      // Payment Method Accordion
      $('input[name="paymentmethod"]').on('click', function () {
        var $value = $(this).attr('value');
        $('.payment-method-details').slideUp();
        $('[data-method="' + $value + '"]').slideDown();
      });
      /*------- Checkout Page accordion start -------*/

      // slide effect dropdown
      function dropdownAnimation() {
        $('.toggle-bar').on('show.bs.toggle-bar', function (e) {
          $(this)
            .find('.dropdown-list')
            .first()
            .stop(true, true)
            .slideDown(5000);
        });

        $('.toggle-bar').on('hide.bs.toggle-bar', function (e) {
          $(this).find('.dropdown-list').first().stop(true, true).slideUp(5000);
        });
      }
      dropdownAnimation();

      // User Changeable Access
      var activeId = $('#instafeed'),
        myTemplate =
          '<div class="instagram-item"><a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /></a><div class="instagram-hvr-content"><span class="tottallikes"><i class="fa fa-heart"></i>{{likes}}</span><span class="totalcomments"><i class="fa fa-comments"></i>{{comments}}</span></div></div>';

      // if (activeId.length) {
      //   var userID = activeId.attr('data-userid'),
      //     accessTokenID = activeId.attr('data-accesstoken'),
      //     userFeed = new Instafeed({
      //       get: 'user',
      //       userId: userID,
      //       accessToken: accessTokenID,
      //       resolution: 'standard_resolution',
      //       template: myTemplate,
      //       sortBy: 'least-recent',
      //       limit: 15,
      //       links: false,
      //     });
      //   userFeed.run();
      // }

      // Instagram feed carousel active
      $(window).on('load', function () {
        var instagramFeed = $('.instagram-carousel');
        instagramFeed.imagesLoaded(function () {
          instagramFeed.slick({
            slidesToShow: 6,
            slidesToScroll: 2,
            dots: false,
            arrows: false,
            responsive: [
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 2,
                },
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 3,
                },
              },
              {
                breakpoint: 991,
                settings: {
                  slidesToShow: 4,
                },
              },
            ],
          });
        });
      });
      var testimonials = document.getElementById('testimonials-section');
      var control1 = document.getElementById('control1');
      var control2 = document.getElementById('control2');
      var control3 = document.getElementById('control3');
      var control4 = document.getElementById('control4');
      var control5 = document.getElementById('control5');
      var control6 = document.getElementById('control6');
      var control7 = document.getElementById('control7');
      var control8 = document.getElementById('control8');

      testimonials.style.transform = 'translateX(3050px)';
      if (window.innerWidth < 500) {
        testimonials.style.transform = 'translateX(1225px)';
      }
      control1.classList.add('active1');
      control2.classList.remove('active1');
      control3.classList.remove('active1');
      control4.classList.remove('active1');
      control5.classList.remove('active1');
      control6.classList.remove('active1');
      control7.classList.remove('active1');
      control8.classList.remove('active1');

      $(control1).on('click', function () {
        testimonials.style.transform = 'translateX(3050px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(1225px)';
        }
        control1.classList.add('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });

      $(control2).on('click', function () {
        testimonials.style.transform = 'translateX(2180px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(880px)';
        }
        control1.classList.remove('active1');
        control2.classList.add('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });

      $(control3).on('click', function () {
        testimonials.style.transform = 'translateX(1310px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(526px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.add('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });
      $(control4).on('click', function () {
        testimonials.style.transform = 'translateX(440px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(176px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.add('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });
      $(control5).on('click', function () {
        testimonials.style.transform = 'translateX(-430px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(-176px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.add('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });
      $(control6).on('click', function () {
        testimonials.style.transform = 'translateX(-1310px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(-526px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.add('active1');
        control7.classList.remove('active1');
        control8.classList.remove('active1');
      });
      $(control7).on('click', function () {
        testimonials.style.transform = 'translateX(-2170px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(-876px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.add('active1');
        control8.classList.remove('active1');
      });
      $(control8).on('click', function () {
        testimonials.style.transform = 'translateX(-3036px)';
        if (window.innerWidth < 500) {
          testimonials.style.transform = 'translateX(-1220px)';
        }
        control1.classList.remove('active1');
        control2.classList.remove('active1');
        control3.classList.remove('active1');
        control4.classList.remove('active1');
        control5.classList.remove('active1');
        control6.classList.remove('active1');
        control7.classList.remove('active1');
        control8.classList.add('active1');
      });
    }, 2000);
  }

  ionViewDidEnter() {}

  changeView(view: any) {
    if (view === 'list') {
      this.viewMode = 'list';
    } else {
      this.viewMode = 'grid';
    }
  }

  chat(text = 'Hi!') {
    window.open(
      'https://api.whatsapp.com/send?phone=' +
        $('#whatsapp').val() +
        '&text=' +
        text,
      '_blank'
    );
    return;
  }

  bookAppointment() {
    this.openBookAppointment();
    return;
  }
  async openBookAppointment() {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: BookAppointmentComponent,
      cssClass: 'fullscreen',
      componentProps: {
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        appointmentDetailsArr: this.storeData.appointment_options,
      },
    });

    modal.dismiss();
    return await modal.present();
  }
  searchStoreName: any = '';
  filteredStores: any = [];
  searchStore() {
    this.filteredStores = this.filterStoreItems(this.searchStoreName);
  }
  filterStoreItems(searchTerm: any) {
    return this.allStores.filter((item) => {
      return (
        item.store_display.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      );
    });
  }
  setFilteredItems() {
    if (!this.searchTerm) {
      this.searchSelectedCategoryProducts = this.selectedCategoryProducts;
    }
    this.searchSelectedCategoryProducts = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm: any) {
    return this.selectedCategoryProducts.filter((item) => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  chosedDeliveryOption: any;
  deliveryOption($event) {
    this.chosedDeliveryOption =
      this.storeData.delivery_options[$event.target.value];
    this.storeData.store.delivery_charges = this.chosedDeliveryOption
      ? +this.chosedDeliveryOption.price
      : this.normalDeliveryCharges;
  }
  normalDeliveryCharges: any;
  allStores: any = [];
  currCategory = 6;
  next() {
    const keys = Object.keys(this.storeData.sub_categories);
    const len = keys.length;
    let category = document.getElementById('category' + this.currCategory);
    let currentIndex = category;

    if (this.currCategory === len + 1) {
      document
        .getElementById('category')
        .scrollIntoView({ behavior: 'smooth' });
      this.currCategory = 4;
    } else {
      this.currCategory = this.currCategory + 1;
      currentIndex.scrollIntoView({ behavior: 'smooth' });
    }
  }
  previous() {
    this.currCategory = this.currCategory - 1;
    let category = document.getElementById('category' + this.currCategory);
    let currentIndex = category;
    if (this.currCategory === 4 || this.currCategory === 0) {
      this.currCategory = 4;
      document
        .getElementById('category')
        .scrollIntoView({ behavior: 'smooth' });
      return;
    }
    currentIndex.scrollIntoView({ behavior: 'smooth' });
  }
  isWhiteLabeled = environment.isWhiteLabeled;
  categories: any = [];
  indexOrderAsc: any;
  getStoreDetails() {
    this.activatedRoute.params.subscribe((res) => {
      this.storeName = res.storeId;

      if (this.storeName === '' || this.storeName === undefined) {
        if (environment.isWhiteLabeled) {
          this.storeName = environment.storeId;
        } else {
          this.router.navigate(['/']);
        }
      }
      let data = {
        store_name: this.storeName,
      };
      this.spinner.show();
      this.store.fetchStoreDetails(data).subscribe(
        (res: any) => {
          if (!res.message) {
            this.spinner.hide();
            this.storeData = [];
            return true;
          }

          this.spinner.hide();
          this.productLoaded = true;
          this.storeData = res.message;
          console.log(this.storeData);

          this.getCartItemsCOF();
          this.getCustomerDetailsCOF();

          if (environment.isWhiteLabeled) {
            if (
              this.storeData.store.hasOwnProperty('store_link_status') &&
              this.storeData.store.hasOwnProperty('is_custom_domain') &&
              this.storeData.store.hasOwnProperty('custom_portal_status')
            ) {
              if (
                this.storeData.store.store_link_status.trim().toLowerCase() ===
                'inactive'
              ) {
                this.storeData.store.store_link_status = 'active';
              }
              if (
                this.storeData.store.is_custom_domain.trim().toLowerCase() ===
                'yes'
              ) {
                if (
                  this.storeData.store.custom_portal_status
                    .trim()
                    .toLowerCase() === 'inactive'
                ) {
                  this.storeData.store.store_link_status = 'inactive';
                }
              }
            }
          }

          this.normalDeliveryCharges = 0;
          this.storeData.store.delivery_charges = 0;

          this.storeData.delivery_options = res.delivery_options;
          this.storeData.bank_options = res.bank_options;
          this.storeData.pickup_days = res.pickup_days;
          this.storeData.appointment_options = res.appointment_options;

          // dynamic og tags
          this.meta.updateTag({
            name: 'description',
            content: this.storeData.store.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: this.storeData.store.logo,
          });
          this.title.setTitle(
            this.storeData.store.display_name + ' - Online store.'
          );
          // dynamic og tags

          if (this.storeData.sub_categories) {
            for (var key in this.storeData.sub_categories) {
              if (this.storeData.sub_categories[key].status) {
                if (this.storeData.sub_categories[key].status === 'active') {
                  this.selectedCategory = this.storeData.sub_categories[key];
                  this.categoryProducts = this.storeData.products;
                  this.changeCategory('ALL');

                  this.indexOrderAsc = (
                    akv: KeyValue<string, any>,
                    bkv: KeyValue<string, any>
                  ): number => {
                    const a = akv.value.order_number;
                    const b = bkv.value.order_number;

                    return a > b ? 1 : b > a ? -1 : 0;
                  };
                  break;
                }
              } else {
                this.selectedCategory = this.storeData.sub_categories[key];
                this.categoryProducts = this.storeData.products;
                this.changeCategory('ALL');

                break;
              }
            }
          }

          this.checkStoreStatus(
            this.storeData.store.opening_time,
            this.storeData.store.closing_time
          );

          $(document).ready(function () {
            function setDeliveryMode(deliveryMode) {
              if (deliveryMode === 'd') {
                this.swalToastMessage('Ordering mode set to delivery', 'info');
              } else {
                this.swalToastMessage('Ordering mode set to pick up', 'info');
              }
            }

            function notifyMessage(themsg, ntype) {
              $('#notification').finish();
              $('#notification').html(themsg);
              $('#notification').removeClass();
              $('#notification').addClass('notification');
              $('#notification').addClass('is-' + ntype);
              $('#notification').fadeIn().delay(800).fadeOut('slow');
            }

            function swalToastMessage(text, icon) {
              const Toast = swal.mixin({
                toast: true,
                background: '#2f3542',
                position: 'bottom-start',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', swal.stopTimer);
                  toast.addEventListener('mouseleave', swal.resumeTimer);
                },
              });
              Toast.fire({
                icon: icon,
                title: text,
                background: '#2f3542',
              });
            }

            $(document).on('click', '.addToCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass('animate');
              $('.scrollToCartFloatingButtonArea .icon-cart').addClass('shake');

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate'
                );
              }, 1000);

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .icon-cart').removeClass(
                  'shake'
                );
              }, 1000);
            });

            $(document).on('click', '.deductFromCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass(
                'animate-out'
              );

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate-out'
                );
              }, 1000);
            });
            var stickyOffset;
            if ($('.container-fluid ul.list-group').offset()) {
              stickyOffset = $('.container-fluid ul.list-group').offset().top;
            }

            $(window).scroll(function () {
              if ($(window).width() <= 991) {
                var sticky = $('.container-fluid ul.list-group');
                var scroll = $(window).scrollTop();

                if (scroll >= stickyOffset) {
                  sticky.addClass('fixedOnTop');
                } else {
                  sticky.removeClass('fixedOnTop');
                }
              }
            });

            $('.container-fluid ul.list-group li:first').addClass('active');
            $(document).on(
              'click',
              '.container-fluid ul.list-group li',
              function () {
                $('.container-fluid ul.list-group li').removeClass('active');
                $(this).addClass('active');
              }
            );

            $(document).on('keyup', '.product-search', function (event) {
              let search_value = $(this).val();
              var loadUrl =
                $('#baseUrl').val() + $('#subDomain').val() + '/product/search';

              $.get(
                loadUrl,
                {
                  search_value: search_value,
                },
                function (response) {
                  $('#primaryData').html(response);
                }
              );
            });

            $(document).on('click', '.addToCart', function () {
              swalToastMessage(
                $(this).attr('name') + ' added to cart!',
                'success'
              );
            });
          });
          console.log(this.storeData.slides);

          // let slideImg = [];
          // let slidesKey = Object.keys(this.storeData.slides);
          // slidesKey.forEach((element) => {
          //   slideImg.push(this.storeData.slides[element].image);
          // });
          // let deviceInfo = this.deviceService.getDeviceInfo();
          // if (deviceInfo.os === 'android' || deviceInfo.os === 'mac') {
          //   this.slides = this.chunk(slideImg, 1);
          // } else {
          //   this.slides = this.chunk(slideImg, 3);
          // }
        },
        (err: any) => {
          this.spinner.hide();
          this.storeData = [];
        }
      );
    });
  }
  shareWw() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://api.whatsapp.com/send?text=' + window.location.href,
      '_blank'
    );
  }
  shareWf() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://www.facebook.com/sharer.php?u=' + window.location.href,
      '_blank'
    );
  }
  shareWt() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://twitter.com/share?url=' + window.location.href,
      '_blank'
    );
  }
  shareWi() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://www.instagram.com/?url=' + window.location.href,
      '_blank'
    );
  }
  shareToWhatsapp(key) {
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');
          if (sName === 'true') {
            window.open(
              'https://api.whatsapp.com/send?text=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            this.storeName = environment.storeId;
            window.open(
              'https://api.whatsapp.com/send?text=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToFacebook(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://www.facebook.com/sharer.php?u=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://www.facebook.com/sharer.php?u=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToInstagram(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://www.instagram.com/?url=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://www.instagram.com/?url=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToTwitter(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://twitter.com/share?url=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://twitter.com/share?url=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  copyMessage(val: any) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    let sName = localStorage.getItem('haveStoreName');

    if (sName === 'true') {
      selBox.value =
        window.location.href +
        '/product/' +
        val.id +
        '\n' +
        val.images1 +
        ' \n' +
        val.name +
        '\n' +
        val.description +
        '\n' +
        'Discount:' +
        val.discount_price +
        ' ' +
        'Price:' +
        val.price;
    } else {
      selBox.value =
        window.location.href +
        'product/' +
        val.id +
        '\n' +
        val.images1 +
        ' \n' +
        val.name +
        '\n' +
        val.description +
        '\n' +
        'Discount:' +
        val.discount_price +
        ' ' +
        'Price:' +
        val.price;
    }

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  chunk(arr: any, chunkSize: any) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  checkStoreStatus(start, end) {
    var timeToCheckMilli = new Date().getTime();
    let startTimeMilli = moment(start, 'HH:mm A').valueOf();
    let endTimeMilli = moment(end, 'HH:mm A').valueOf();
    if (
      timeToCheckMilli >= startTimeMilli &&
      timeToCheckMilli <= endTimeMilli
    ) {
      this.storeOpenStatus = 'Open';
    } else {
      this.storeOpenStatus = 'Closed';
    }
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }
  scrollToElement(): void {
    document.getElementById('target').scrollIntoView({ behavior: 'smooth' });
  }
  scrollToElementCD() {
    document.getElementById('cDetails').scrollIntoView({ behavior: 'smooth' });
  }
  toggle = true;
  menuToggle() {
    this.toggle = !this.toggle;
  }
  hambergerToggle = true;
  hambergerMenuToggle() {
    this.hambergerToggle = !this.hambergerToggle;
  }
  isReview = false;
  changeCategory(index: any) {
    this.isReview = false;
    if (index === 'reviews') {
      if (this.storeData && this.storeData.reviews) {
        if (this.common.sizeOfObj(this.storeData.reviews)) {
          this.selectedCategory = this.storeData.reviews;
          this.selectedCategoryProducts = [];
          for (const key in this.storeData.reviews) {
            this.selectedCategoryProducts.push(this.storeData.reviews[key]);
          }
          this.isReview = true;
          this.setFilteredItems();
        }
      } else {
        this.selectedCategoryProducts = [];
        this.setFilteredItems();
        this.isReview = false;
      }

      return;
    } else if (index === 'ALL') {
      if (this.storeData && this.storeData.sub_categories) {
        if (this.common.sizeOfObj(this.storeData.sub_categories)) {
          this.selectedCategory = 'ALL';
          this.selectedCategoryProducts = [];
          const activeCategories = this.common.getActiveCategories(
            this.storeData.sub_categories
          );

          for (const key in this.categoryProducts) {
            if (
              activeCategories.includes(
                this.categoryProducts[key].sub_category_id.toString()
              )
            ) {
              this.selectedCategoryProducts.push(this.categoryProducts[key]);
            }
          }

          this.setFilteredItems();
        }
      }
    } else {
      if (this.storeData && this.storeData.sub_categories) {
        if (this.common.sizeOfObj(this.storeData.sub_categories)) {
          this.selectedCategory = this.storeData.sub_categories[index];
          this.selectedCategoryProducts = [];

          for (const key in this.categoryProducts) {
            if (
              this.selectedCategory.id ===
              this.categoryProducts[key].sub_category_id
            ) {
              this.selectedCategoryProducts.push(this.categoryProducts[key]);
            }
          }

          this.setFilteredItems();
        }
      }
    }
  }
  scrollToLabel(label) {
    var titleELe = document.getElementById(label);
    this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
  }

  total: number = 0;
  payable: number = 0;

  addcartProducts: any = [];

  addToCartCOF(product: any) {
    this.addcartProducts = product;
    this.addcartProducts.images = product.images1;
    this.addcartProducts.quantity = 1;
    this.addcartProducts.product_payable_price = product.price;

    this.storageService.setStorageValueCOF(
      this.addcartProducts,
      this.storeData.store.name.trim() + '-' + 'my-cart-cof'
    );
    this.swalToastMessage(product.name + ' added to cart!', 'success');
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 1500);
  }
  shareContent = {
    url: window.location.href,
  };
  removeFromCart(product: any) {
    this.storageService.removeStorageValue(
      product,
      this.storeData.store.name.trim() + '-' + 'my-cart-cof'
    );
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 800);
  }
  removeExtraFromCart(product: any, extraIndex: any) {
    this.storageService.removeExtraStorageValue(
      product,
      extraIndex,
      this.storeData.store.name.trim() + '-' + 'my-cart-cof'
    );
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 800);
  }

  getExtraName(item) {
    return item.product_extras[Object.keys(item.product_extras)[0]].name;
  }
  getCartItemsCOF() {
    this.storageService
      .getStorageCOF(this.storeData.store.name.trim() + '-' + 'my-cart-cof')
      .then((products) => {
        if (products && products.length) {
          this.cartProducts = products;
          this.total = 0;
          for (var i = 0; i < this.cartProducts.length; i++) {
            this.total +=
              this.cartProducts[i].discount_price *
              this.cartProducts[i].quantity;
            this.loadingCart = false;
          }
        } else {
          this.cartProducts = [];
          this.loadingCart = false;
        }
      });
  }
  getCustomerDetailsCOF() {
    this.storageService
      .getStorageCOF('my-customer-cof')
      .then((details) => {
        if (details && details.length) {
          this.addCustomerDetails = details;
          if (this.addCustomerDetails[0].selectedDelivery.trim() === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        } else {
          this.addCustomerDetails = [];
        }
      })
      .catch((err) => (this.addCustomerDetails = []));
  }
  minusQuantityCOF(product: any, event: any) {
    this.storageService
      .getStorageCOF(this.storeData.store.name.trim() + '-' + 'my-cart-cof')
      .then((products) => {
        this.cartProducts = products;
        let items = this.cartProducts;
        if (!items || items.length === 0) {
          this.swalToastMessage(product.name + ' not found in cart!', 'error');
          return null;
        }

        let isNew = true;
        let k = 0;
        for (let i of items) {
          if (i.id === product.id) {
            let newQuantity = i.quantity - 1;
            i.quantity = newQuantity;
            isNew = false;
            product.quantity = newQuantity;
            product = product;
          }
          k++;
        }

        if (!isNew) {
          if (product.quantity >= 1) {
            this.storageService.updateStorageValueCOF(
              product,
              null,
              this.storeData.store.name.trim() + '-' + 'my-cart-cof'
            );
          } else {
            this.storageService.removeStorageValueCOF(
              product.id,
              null,
              this.storeData.store.name.trim() + '-' + 'my-cart-cof'
            );
          }
          this.swalToastMessage(
            product.name + ' removed from cart!',
            'success'
          );
        } else {
          this.swalToastMessage(product.name + ' not found in cart!', 'error');
        }

        setTimeout(() => {
          this.getCartItemsCOF();
        }, 300);
      });
  }

  notifyMessage(themsg, ntype) {
    $('#notification').finish();
    $('#notification').html(themsg);
    $('#notification').removeClass();
    $('#notification').addClass('notification');
    $('#notification').addClass('is-' + ntype);
    $('#notification').fadeIn().delay(800).fadeOut('slow');
  }

  swalToastMessage(text, icon, timer = 3000) {
    const Toast = swal.mixin({
      toast: true,
      background: '#2f3542',
      position: 'bottom-start',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      },
    });
    Toast.fire({
      icon: icon,
      title: text,
      background: '#2f3542',
    });
  }

  generateOrder() {
    var mode = $('#selectedDelivery').val();

    if ($('#contactNo').val().trim() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      return false;
    }

    if ($('#name').val().trim() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val().trim() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        return false;
      }
      if ($('#address2').val().trim() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        return false;
      }
    }

    var loadUrl = $('#baseUrl').val() + $('#subDomain').val() + '/cart-order';
    var href = '';
    $.ajax({
      url: loadUrl,
      method: 'GET',
      async: false,
      context: this,
      data: {
        selected_delivery: $('#selectedDelivery').val(),
        name: $('#name').val(),
        contact_no: $('#contactNo').val(),
        address1: $('#address1').val(),
        address2: $('#address2').val(),
        pincode: '',
        notes: $('#notes').val(),
      },
      success: function (data) {
        data = JSON.parse(data);

        var orderContent = '';
        this.getCartItemsCOF();

        if (this.cartProducts && this.cartProducts.length) {
          var itemsList = '';
          for (var i = 0; i < this.cartProducts.length; i++) {
            itemsList =
              itemsList +
              '▪️' +
              this.cartProducts[i].name +
              ' x ' +
              this.cartProducts[i].quantity +
              ' x ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.cartProducts[i].discount_price +
              '  = ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              +(
                this.cartProducts[i].discount_price *
                this.cartProducts[i].quantity
              ) +
              '\n';
          }

          this.specialInstructions =
            `Special Instructions : ` + $('#notes').val() + `\n------\n`;

          if ($('#selectedDelivery').val() == 'd') {
            orderContent =
              `✅ Delivery Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity | Price | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details :\nName : ` +
              this.storeData.store.display_name +
              `\nDelivery Option : ` +
              this.delivery_options.name
                ? this.delivery_options.name
                : 'Default' +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
          } else {
            orderContent =
              `✅ Pickup Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity | Price | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details For Pickup:\nName : ` +
              this.storeData.store.display_name +
              `\nContact : ` +
              this.storeData.store.whatsapp_number +
              `\nAddress : ` +
              this.storeData.store.address +
              `\n------\n` +
              this.specialInstructions +
              this.storeData.store.display_name +
              ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details :\nName : ` +
              $('#name').val() +
              `\nContact : ` +
              $('#contactNo').val() +
              `\n------\nHave a nice day, Thank you :)\n`;
          }
        }

        orderContent = encodeURI(orderContent);
        var isSafari =
          navigator.vendor &&
          navigator.vendor.indexOf('Apple') > -1 &&
          navigator.userAgent &&
          navigator.userAgent.indexOf('CriOS') == -1 &&
          navigator.userAgent.indexOf('FxiOS') == -1;
        if (isSafari) {
          href =
            'https://wa.me/' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        } else {
          href =
            'https://api.whatsapp.com/send?phone=' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        }
      },
    }).done(function () {
      window.open(href, '_blank');
    });
  }

  addCustomerDetails: any = [];
  addQuantityCOF(product, index) {
    if (product.quantity) {
      product.quantity = product.quantity + 1;
    } else {
      product.quantity = 1;
      product.quantity = product.quantity + 1;
    }
  }

  removeProductCOF(product, index) {
    this.cartProducts.splice(index, 1);
    this.storageService.removeStorageValueCOF(
      product.id,
      null,
      this.storeData.store.name.trim() + '-' + 'my-cart-cof'
    );

    setTimeout(() => {
      this.spinner.show();
      this.getCartItemsCOF();
      this.spinner.hide();
    }, 300);
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  goToCreateOrderOptions() {
    this.spinner.show();
    var mode = $('#selectedDelivery').val();

    if ($('#delivery-mode').val() === 'none') {
      this.swalToastMessage('Please choose order preference', 'warning');
      this.spinner.hide();
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if (this.storeData.delivery_options.length !== 0) {
        if ($('#delivery_option').val().trim() == 'none') {
          this.swalToastMessage('Please choose delivery option', 'warning');
          this.spinner.hide();
          return false;
        }
      }
    }

    if (this.storeData.bank_options.length !== 0) {
      if ($('#bank_option').val().trim() == 'none') {
        this.swalToastMessage(
          'Please choose preferred mode of payment',
          'warning'
        );
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactEmail').val().trim() == '') {
      this.swalToastMessage('Please enter your email address', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test($('#contactEmail').val().trim()) == false) {
        this.swalToastMessage('Please enter valid email address', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactNo').val().trim() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var phoneNum = $('#contactNo').val().trim().replace(/[^\d]/g, '');
      if (phoneNum.length > 6 && phoneNum.length < 12) {
      } else {
        this.swalToastMessage('Please enter valid contact number', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#name').val().trim() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      this.spinner.hide();
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val().trim() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }

      if ($('#address2').val().trim() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if (
      this.storeData.pickup_days.length !== 0 &&
      $('#selectedDelivery').val() == 'p'
    ) {
      if ($('#pickup_day').val().trim() == 'none') {
        this.swalToastMessage('Please choose pickup day', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    var custDetails = {
      selectedDelivery: $('#selectedDelivery').val().trim(),
      name: $('#name').val().trim(),
      contactNo: $('#contactNo').val().trim(),
      contactEmail: $('#contactEmail').val().trim(),
      address1: $('#address1').val().trim(),
      address2: $('#address2').val().trim(),
      pincode: '',
      notes: $('#notes').val().trim(),
    };

    this.storageService.clearStorageValueCOF('my-customer-cof').then((res) => {
      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }
      this.storageService.setCustomerStorageValueCOF(
        custDetails,
        'my-customer-cof'
      );
      this.addCustomerDetails[0] = custDetails;
    });

    let data = JSON.stringify({
      account_id: this.storeData.store.account_email_id,
      store_name: this.storeName,
      email: this.storeData.store.account_email_id,
      selected_delivery: $('#selectedDelivery').val(),
      name: $('#name').val(),
      contact_no: $('#contactNo').val(),
      contact_email: $('#contactEmail').val(),
      address1: $('#address1').val(),
      address2: $('#address2').val(),
      pincode: '',
      notes: $('#notes').val(),

      currency_symbol: this.storeData.store.currency_symbol,
      total: this.total,
      delivery_charges:
        $('#selectedDelivery').val() === 'd'
          ? this.storeData.store.delivery_charges
          : 0, //new
      delivery_options: this.chosedDeliveryOption
        ? this.chosedDeliveryOption
        : 'false', //new
      bank_options: this.storeData.bank_options[$('#bank_option').val()]
        ? this.storeData.bank_options[$('#bank_option').val()]
        : 'N/A', //new

      pickup_day:
        $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
          ? $('#pickup_day').val()
          : 'N/A', //new

      is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
      coupon_discount_value: this.appliedCouponDetails
        ? (
            (this.total * this.appliedCouponDetails.discount_percentage) /
            100
          ).toFixed(2)
        : 0, //new
      coupon_details: this.appliedCouponDetails
        ? this.appliedCouponDetails
        : 'false', //new
      payable:
        $('#selectedDelivery').val() === 'd'
          ? (
              this.total +
              (this.storeData.store.delivery_charges
                ? this.storeData.store.delivery_charges
                : 0) -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2)
          : (
              this.total -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2), //new
      items: this.cartProducts,
    });

    this.spinner.show();
    this.store.placeOrderWithOptionsAndExtras(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          let data = {
            mode: 'WP',
          };
          if (data.mode === 'WP') {
            var orderContent = '';
            this.getCartItemsCOF();

            if (this.cartProducts && this.cartProducts.length) {
              var itemsList = '';
              for (var i = 0; i < this.cartProducts.length; i++) {
                itemsList =
                  itemsList +
                  '▪️' +
                  this.cartProducts[i].name +
                  ' x ' +
                  this.cartProducts[i].quantity +
                  ' x ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.cartProducts[i].discount_price +
                  '  = ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.cartProducts[i].discount_price *
                    this.cartProducts[i].quantity +
                  '\n';
              }

              this.specialInstructions =
                `Special Instructions : ` + $('#notes').val() + `\n------\n`;

              if ($('#selectedDelivery').val() == 'd') {
                orderContent =
                  `✅ Delivery Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Price | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nDelivery Charges : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.storeData.store.delivery_charges
                    ? +this.storeData.store.delivery_charges
                    : 0
                  ).toFixed(2) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        (this.storeData.store.delivery_charges
                          ? +this.storeData.store.delivery_charges
                          : 0) -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nDelivery Partner : \n` +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.name
                    : 'Default') +
                  ' ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.price
                    : 0) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nStore Details :\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              } else {
                orderContent =
                  `✅ Pickup Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Price | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        this.storeData.store.delivery_charges -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nPreferred pickup day : \n` +
                  ($('#pickup_day').val() && $('#pickup_day').val() !== 'none'
                    ? $('#pickup_day').val()
                    : 'N/A') +
                  `\n------\nStore Details For Pickup:\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              }

              this.orderContent = orderContent;

              this.orderContent = this.orderContent.replace(
                '__ORDER__ID__',
                res.order_id
              );
            }

            orderContent = encodeURI(orderContent);
            var isSafari =
              navigator.vendor &&
              navigator.vendor.indexOf('Apple') > -1 &&
              navigator.userAgent &&
              navigator.userAgent.indexOf('CriOS') == -1 &&
              navigator.userAgent.indexOf('FxiOS') == -1;
            if (isSafari) {
              this.href =
                'https://wa.me/' +
                $('#whatsapp').val() +
                '?text=' +
                orderContent.toString();
            } else {
              this.href =
                'https://api.whatsapp.com/send?phone=' +
                $('#whatsapp').val() +
                '&text=' +
                orderContent.toString();
            }
          } else if (data.mode == 'SMS') {
            this.href =
              'sms://' +
              $('#whatsapp').val() +
              ';?&body=' +
              orderContent.toString();
          }

          setTimeout(() => {
            this.getCartItemsCOF();
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
            this.openOrderDetails();
            this.spinner.hide();
          }, 1000);
        } else {
          this.swalToastMessage(
            'Unable to create order, try again later!',
            'warning'
          );
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage(
          'Unable to create order, try again later!',
          'warning'
        );
      }
    );
  }
  async openOrderDetails() {
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    let modal = await this.modalController.create({
      component: CreateOrderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        cartArr: this.cartProducts,
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        totalAmount: this.total,
        waHref: this.href,
        orderContent: this.orderContent,
        accountEmail: this.storeData.store.account_email_id,
        storeName: this.storeName,
        delivery_charges:
          $('#selectedDelivery').val() === 'd'
            ? this.storeData.store.delivery_charges
            : 0, //new
        delivery_options: this.chosedDeliveryOption
          ? this.chosedDeliveryOption
          : 'false', //new
        bank_options: this.storeData.bank_options[$('#bank_option').val()]
          ? this.storeData.bank_options[$('#bank_option').val()]
          : 'N/A', //new
        pickup_day:
          $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
            ? $('#pickup_day').val()
            : 'N/A', //new
        is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
        coupon_discount_value: this.appliedCouponDetails
          ? (
              (this.total * this.appliedCouponDetails.discount_percentage) /
              100
            ).toFixed(2)
          : 0, //new
        coupon_details: this.appliedCouponDetails
          ? this.appliedCouponDetails
          : 'false', //new
        payable:
          $('#selectedDelivery').val() === 'd'
            ? (
                this.total +
                this.storeData.store.delivery_charges -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2)
            : (
                this.total -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2), //new
        items: this.cartProducts,
      },
    });

    this.storageService
      .clearStorageValueCOF(
        this.storeData.store.name.trim() + '-' + 'my-cart-cof'
      )
      .then((res) => {
        this.swalToastMessage(
          'Your order taken successfully, You will receive call from the ' +
            this.storeData.store.display_name +
            ' team shortly.',
          'success',
          6000
        );
        this.getCartItemsCOF();
        this.appliedCouponDetails = false;
        this.couponCode = null;

        if (this.addCustomerDetails) {
          if (this.addCustomerDetails[0].selectedDelivery.trim() === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        }

        if (this.storeData.store) {
          if (this.storeData.store.order_mode) {
            if (
              this.storeData.store.order_mode === 'all' ||
              this.storeData.store.order_mode === 'delivery'
            ) {
              $('.show-address').show();
              $('.show-pickup-total').hide();
              $('#selectedDelivery').val('d');
              $('#delivery-mode').val('d');
              $('#pickup').removeClass('active');
              $('#delivery').addClass('active');
            } else {
              $('.show-address').hide();
              $('.show-pickup-total').show();
              $('#selectedDelivery').val('p');
              $('#delivery-mode').val('p');
              $('#pickup').addClass('active');
              $('#delivery').removeClass('active');
            }
          }
        }
      });

    modal.dismiss();
    return await modal.present();
  }
  items: any = [];
  imgClicked = false;
  imgVal: any;
  @ViewChild('imgModal', { read: ElementRef, static: false })
  imgModal: ElementRef;
  imgIndex: any;
  nextBtn = true;
  previousBtn = true;
  openImg(item) {
    this.imgClicked = true;
    this.imgVal = item;
  }

  close() {
    this.imgModal.nativeElement.style.display = 'none';
    this.imgClicked = false;
    this.previousBtn = true;
    this.nextBtn = true;
  }
  viewImageSlider(image, additionalImagesArr) {
    if (additionalImagesArr) {
      additionalImagesArr['main'] = { image: image };
    } else {
      additionalImagesArr = { main: { image: image } };
    }

    this.openImageSlider(additionalImagesArr);
    return;
  }
  async openImageSlider(additionalImagesArr) {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: ImageSliderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        additionalImagesArr: additionalImagesArr,
      },
    });

    modal.dismiss();
    return await modal.present();
  }
  goBack() {
    this.location.back();
  }
  goToCart() {
    this.router.navigate(['/cart/' + this.storeName]).then((res) => {
      this.getCartItemsCOF();
    });
  }

  gotoTop() {
    this.content.scrollToTop(0);
  }
  gotoSpeficCategory(itemKey: any) {
    // this.router.navigate(['/']).then((res) => {
    //   this.router.navigate(['/products/' + itemKey]);
    // });
    this.router.navigate(['/products/' + itemKey]);
  }
}
