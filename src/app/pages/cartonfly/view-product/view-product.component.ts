import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ModalController, IonContent } from '@ionic/angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageService } from '../../../services/storage.service';
import { StoreService } from '../../../services/store.service';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { CreateOrderComponent } from '../../../pages/cartonfly/create-order/create-order.component';
import { BookAppointmentComponent } from '../../../pages/cartonfly/book-appointment/book-appointment.component';

import { ImageSliderComponent } from '../../../pages/cartonfly/image-slider/image-slider.component';

import { ActivatedRoute, Router } from '@angular/router';
import {
  environment,
  httpBasicAuthOptions,
} from '../../../../environments/environment';
import { empty } from 'rxjs';
import { CurrencySymbolPipe } from '../../../currency-symbol.pipe';
import * as introJs from 'intro.js/intro.js';

import { Meta, Title } from '@angular/platform-browser';
import { exit } from 'process';
import { Ng2DeviceService } from 'ng2-device-detector';
import { Location } from '@angular/common';
@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss'],
})
export class ViewProductComponent implements OnInit {
  @ViewChild(IonContent, {
    static: false,
  })
  content: IonContent;
  storeName: any;
  href: any;
  orderContent: any;
  storeOpenStatus: any;
  storeData: any = [];
  cartProducts: any = [];
  selectedCategoryProducts: any = [];
  searchSelectedCategoryProducts: any = [];
  searchTerm = '';
  selectedCategory: any = [];
  categoryProducts: any = [];
  specialInstructions: any;
  viewMode: any;
  couponCode: any;
  appliedCouponDetails: any;
  slides: any = [[]];
  introJS = introJs();
  loading = true;
  loadingCart = true;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  prodCount: any;
  productLoaded: boolean = false;
  checkInCart(product: any) {
    let flag = false;
    let prodCount = 0;
    if (this.cartProducts && this.cartProducts.length) {
      for (var i = 0; i < this.cartProducts.length; i++) {
        if (product.id === this.cartProducts[i].id) {
          flag = true;
          prodCount = this.cartProducts[i].quantity;
        }
      }
    } else {
      flag = false;
      prodCount = 0;
    }

    if (prodCount) {
      $('.quantityCountProduct' + product.id).html('x' + prodCount);
      $('.quantityCountProduct' + product.id).show();
    } else {
      $('.quantityCountProduct' + product.id).hide();
    }
    this.prodCount = prodCount;
    return prodCount;
  }

  introMethod() {
    this.introJS.setOptions({
      steps: [
        {
          element: '#step1',
          intro:
            'Welcome to our online! <br>Let me guide you on creating order.',
        },
        {
          element: '#step2',
          intro: 'Click here to chat with our store owner.',
          position: 'right',
        },
        {
          element: '#step3',
          intro: 'Choose categories from here.',
          position: 'bottom',
        },
        {
          element: '#step4',
          intro: 'Search for the products from selected categories here.',
          position: 'bottom',
        },
        {
          element: '#step5',
          intro: 'Add/Minus products to the cart from here.',
          position: 'bottom',
        },
        {
          element: '#step6',
          intro: 'Check your cart items here.',
          position: 'bottom',
        },

        {
          element: '#step7a',
          intro: 'Choose your order mode.',
          position: 'bottom',
        },
        {
          element: '#step7b',
          intro: 'Provide your details here.',
          position: 'bottom',
        },
        {
          element: '#step8',
          intro: 'Click here to generate order.',
          position: 'bottom',
        },
      ],
      showProgress: true,
      skipLabel: 'Skip',
      prevLabel: 'Prev',
      nextLabel: 'Next',
      doneLabel: 'Complete',
      overlayOpacity: '0',
    });

    this.introJS.start();
    this.introJS.oncomplete(function () {
      localStorage.setItem('introForStoreActivate', 'true');
    });
  }

  slideOpts = {
    initialSlide: 0,
    loop: true,
    autoplay: true,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };

  checkoutNow() {
    $('#contactNo').focus();
    this.swalToastMessage(
      'Choose your delivery mode & enter required details to create order.',
      'info'
    );
  }
  goBack() {
    // this.router.navigate([this.storeName]);
    this.location.back();
  }
  constructor(
    private store: StoreService,
    private modalController: ModalController,
    private spinner: NgxSpinnerService,
    public storageService: StorageService,
    public common: CommonService,
    private ref: ChangeDetectorRef,
    private router: Router,
    public meta: Meta,
    public title: Title,
    private activatedRoute: ActivatedRoute,
    private deviceService: Ng2DeviceService,
    private location: Location
  ) {
    this.appliedCouponDetails = false;
    this.viewMode = 'grid';
    this.spinner.show();
    this.cartProducts = [];
    this.specialInstructions = 'N/A';

    if (environment.isWhiteLabeled) {
      if (!localStorage.getItem('storeId')) {
        localStorage.setItem('storeId', environment.storeId);
        this.storeName = localStorage.getItem('storeId');
      } else {
        this.storeName = localStorage.getItem('storeId');
      }
    } else {
      if ((this.storeName = this.router.url.split('/')[1] !== '#')) {
        if ((this.storeName = this.router.url.split('/')[1] === 'store')) {
          this.storeName = this.router.url.split('/')[2];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      } else {
        if ((this.storeName = this.router.url.split('/')[2] === 'store')) {
          this.storeName = this.router.url.split('/')[3];
        } else {
          this.storeName = this.router.url.split('/')[1];
        }
      }
    }

    this.getStoreDetails();
    $('.container-fluid ul.list-group li:first').trigger('click');
    this.spinner.hide();

    function swalToastMessage(text, icon) {
      const Toast = swal.mixin({
        toast: true,
        background: '#2f3542',
        position: 'bottom-start',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer);
          toast.addEventListener('mouseleave', swal.resumeTimer);
        },
      });
      Toast.fire({
        icon: icon,
        title: text,
        background: '#2f3542',
      });
    }

    function setDeliveryMode(deliveryMode) {
      if (deliveryMode === 'd') {
        swalToastMessage('Ordering mode set to delivery', 'info');
      } else {
        swalToastMessage('Ordering mode set to pick up', 'info');
      }
    }

    $('#delivery').click(function () {
      $('.show-address').show();
      $('.show-pickup-total').hide();
      $('#selectedDelivery').val('d');
      $('#delivery-mode').val('d');
      $('#pickup').removeClass('active');
      $('#delivery').addClass('active');
      setDeliveryMode('d');
    });

    $('#pickup').click(function () {
      $('.show-address').hide();
      $('.show-pickup-total').show();
      $('#selectedDelivery').val('p');
      $('#delivery-mode').val('p');
      $('#pickup').addClass('active');
      $('#delivery').removeClass('active');
      setDeliveryMode('p');
    });

    if (this.storeData.store) {
      if (this.storeData.store.order_mode) {
        if (
          this.storeData.store.order_mode === 'all' ||
          this.storeData.store.order_mode === 'delivery'
        ) {
          $('.show-address').show();
          $('.show-pickup-total').hide();
          $('#selectedDelivery').val('d');
          $('#pickup').removeClass('active');
          $('#delivery').addClass('active');
        } else {
          $('.show-address').hide();
          $('.show-pickup-total').show();
          $('#selectedDelivery').val('p');
          $('#pickup').addClass('active');
          $('#delivery').removeClass('active');
        }
      }
    }

    $(window).resize(function () {});
  }
  singleProduct: any;
  productId: any;
  ngOnInit() {}

  changeView(view: any) {
    if (view === 'list') {
      this.viewMode = 'list';
    } else {
      this.viewMode = 'grid';
    }
  }

  chat(text = 'Hi!') {
    window.open(
      'https://api.whatsapp.com/send?phone=' +
        $('#whatsapp').val() +
        '&text=' +
        text,
      '_blank'
    );
    return;
  }

  bookAppointment() {
    this.openBookAppointment();
    return;
  }
  async openBookAppointment() {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: BookAppointmentComponent,
      cssClass: 'fullscreen',
      componentProps: {
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        appointmentDetailsArr: this.storeData.appointment_options,
      },
    });

    modal.dismiss();
    return await modal.present();
  }

  productOptionDetailsArr: any;
  productExtraDetailsArr: any;

  pageType: any = 'product';
  chosedDeliveryOption: any;
  deliveryOption($event) {
    this.chosedDeliveryOption =
      this.storeData.delivery_options[$event.target.value];
    this.storeData.store.delivery_charges = this.chosedDeliveryOption
      ? +this.chosedDeliveryOption.price
      : this.normalDeliveryCharges;
  }

  productPayablePrice: any;

  extrasClick(e, extra, key) {
    if (e.target.checked) {
      e.target.checked = true;
      if (!this.singleProduct.hasOwnProperty('has_extras')) {
        this.singleProduct.has_extras = [];
      }
      extra.value.id = key;
      this.singleProduct.has_extras.push(extra.value);
    } else {
      e.target.checked = false;
      this.singleProduct.has_extras.splice(extra.value, 1);

      if (this.singleProduct.hasOwnProperty('has_extras')) {
        if (!this.singleProduct.has_extras.length) {
          delete this.singleProduct.has_extras;
        }
      }
    }

    this.productPayablePrice = this.common.getProductPayablePrice(
      this.productPayablePrice,
      this.singleProduct
    );
  }
  optionsChange(e, option, key) {
    if (!this.singleProduct.hasOwnProperty('has_options')) {
      delete this.singleProduct.has_options;
    }
    if (e.target.value && e.target.value !== 'none') {
      this.singleProduct.has_options = option[e.target.value];
      this.singleProduct.has_options.id = e.target.value;

      if (option[e.target.value].image) {
        this.viewImg(option[e.target.value].image);
      } else {
        this.viewImg(this.common.defaultImg);
      }
    } else {
      delete this.singleProduct.has_options;
      this.viewImg(this.common.defaultImg);
    }
    this.productPayablePrice = this.common.getProductPayablePrice(
      this.productPayablePrice,
      this.singleProduct
    );
  }

  normalDeliveryCharges: any;
  getStoreDetails() {
    this.activatedRoute.paramMap.subscribe((res) => {
      this.productId = res.get('productId').split('&');
      let data = {
        id: this.productId[0],
        store_name: this.productId[1],
      };
      this.storeName = this.productId[1];

      this.pageType = this.router.url.split('/')[1];

      if (this.pageType === 'product') {
        this.store.getProductById(data).subscribe((res) => {
          this.singleProduct = res.message;

          this.productPayablePrice = this.common.getProductPayablePrice(
            this.productPayablePrice,
            this.singleProduct
          );

          if (res.message.product_options) {
            this.productOptionDetailsArr =
              res.message.product_options[
                Object.keys(res.message.product_options)[0]
              ];
          }
          if (res.message.product_extras) {
            this.productExtraDetailsArr =
              res.message.product_extras[
                Object.keys(res.message.product_extras)[0]
              ];
          }
          this.imgValue = this.singleProduct.images1;
          this.common.defaultImg = this.singleProduct.images1;
          this.loading = false;
        });
      } else {
        this.isReview = true;
        this.store.getReviewById(data).subscribe((res) => {
          this.singleProduct = res.message;
          this.imgValue = this.singleProduct.images1;

          this.common.defaultImg = this.singleProduct.images1;
          this.loading = false;
        });
      }

      let data2 = {
        store_name: this.storeName,
      };
      this.store.fetchStoreDetails(data2).subscribe(
        (res: any) => {
          if (!res.message) {
            this.spinner.hide();
            this.storeData = [];
            return true;
          }

          this.spinner.hide();
          this.productLoaded = true;
          this.storeData = res.message;

          this.normalDeliveryCharges = 0;
          this.storeData.store.delivery_charges = 0;

          this.storeData.delivery_options = res.delivery_options;
          this.storeData.bank_options = res.bank_options;
          this.storeData.pickup_days = res.pickup_days;
          this.storeData.appointment_options = res.appointment_options;

          this.getCartItemsCOF();
          this.getCustomerDetailsCOF();

          //dynamic og tags
          this.meta.updateTag({
            name: 'description',
            content: this.storeData.store.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: this.storeData.store.logo,
          });
          this.title.setTitle(
            this.storeData.store.display_name + ' - Online store.'
          );
          //dynamic og tags

          if (this.storeData.sub_categories) {
            for (var key in this.storeData.sub_categories) {
              if (this.storeData.sub_categories[key].status) {
                if (this.storeData.sub_categories[key].status === 'active') {
                  this.selectedCategory = this.storeData.sub_categories[key];
                  this.categoryProducts = this.storeData.products;
                  break;
                }
              } else {
                this.selectedCategory = this.storeData.sub_categories[key];
                this.categoryProducts = this.storeData.products;
                break;
              }
            }
          }

          this.checkStoreStatus(
            this.storeData.store.opening_time,
            this.storeData.store.closing_time
          );

          $(document).ready(function () {
            function setDeliveryMode(deliveryMode) {
              if (deliveryMode === 'd') {
                this.swalToastMessage('Ordering mode set to delivery', 'info');
              } else {
                this.swalToastMessage('Ordering mode set to pick up', 'info');
              }
            }

            function notifyMessage(themsg, ntype) {
              $('#notification').finish();
              $('#notification').html(themsg);
              $('#notification').removeClass();
              $('#notification').addClass('notification');
              $('#notification').addClass('is-' + ntype);
              $('#notification').fadeIn().delay(800).fadeOut('slow');
            }

            function swalToastMessage(text, icon) {
              const Toast = swal.mixin({
                toast: true,
                background: '#2f3542',
                position: 'bottom-start',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', swal.stopTimer);
                  toast.addEventListener('mouseleave', swal.resumeTimer);
                },
              });
              Toast.fire({
                icon: icon,
                title: text,
                background: '#2f3542',
              });
            }

            $(document).on('click', '.addToCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass('animate');
              $('.scrollToCartFloatingButtonArea .icon-cart').addClass('shake');

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate'
                );
              }, 1000);

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .icon-cart').removeClass(
                  'shake'
                );
              }, 1000);
            });

            $(document).on('click', '.deductFromCart', function () {
              $('.scrollToCartFloatingButtonArea .ball').addClass(
                'animate-out'
              );

              setTimeout(function () {
                $('.scrollToCartFloatingButtonArea .ball').removeClass(
                  'animate-out'
                );
              }, 1000);
            });
            var stickyOffset;
            if ($('.container-fluid ul.list-group').offset()) {
              stickyOffset = $('.container-fluid ul.list-group').offset().top;
            }

            $(window).scroll(function () {
              if ($(window).width() <= 991) {
                var sticky = $('.container-fluid ul.list-group');
                var scroll = $(window).scrollTop();

                if (scroll >= stickyOffset) {
                  sticky.addClass('fixedOnTop');
                } else {
                  sticky.removeClass('fixedOnTop');
                }
              }
            });

            $('.container-fluid ul.list-group li:first').addClass('active');
            $(document).on(
              'click',
              '.container-fluid ul.list-group li',
              function () {
                $('.container-fluid ul.list-group li').removeClass('active');
                $(this).addClass('active');
              }
            );

            $(document).on('keyup', '.product-search', function (event) {
              let search_value = $(this).val();
              var loadUrl =
                $('#baseUrl').val() + $('#subDomain').val() + '/product/search';

              $.get(
                loadUrl,
                {
                  search_value: search_value,
                },
                function (response) {
                  $('#primaryData').html(response);
                }
              );
            });

            $(document).on('click', '.addToCart', function () {
              swalToastMessage(
                $(this).attr('name') + ' added to cart!',
                'success'
              );
            });
          });
          let slideImg = [];
          let slidesKey = Object.keys(this.storeData.slides);
          slidesKey.forEach((element) => {
            slideImg.push(this.storeData.slides[element].image);
          });
          let deviceInfo = this.deviceService.getDeviceInfo();
          if (deviceInfo.os === 'android' || deviceInfo.os === 'mac') {
            this.slides = this.chunk(slideImg, 1);
          } else {
            this.slides = this.chunk(slideImg, 3);
          }
        },
        (err: any) => {
          this.spinner.hide();
          this.storeData = [];
        }
      );
    });
  }
  shareWw() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://api.whatsapp.com/send?text=' + window.location.href,
      '_blank'
    );
  }
  shareWf() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://www.facebook.com/sharer.php?u=' + window.location.href,
      '_blank'
    );
  }
  shareWt() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://twitter.com/share?url=' + window.location.href,
      '_blank'
    );
  }
  shareWi() {
    this.meta.updateTag({
      name: 'description',
      content: this.storeData.store.description,
    });
    this.meta.updateTag({
      name: 'image',
      content: this.storeData.store.logo,
    });
    this.title.setTitle(this.storeData.store.display_name + ' - Online store.');
    window.open(
      'https://www.instagram.com/?url=' + window.location.href,
      '_blank'
    );
  }
  shareToWhatsapp(key) {
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;

      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');
          if (sName === 'true') {
            window.open(
              'https://api.whatsapp.com/send?text=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            this.storeName = environment.storeId;
            window.open(
              'https://api.whatsapp.com/send?text=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToFacebook(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://www.facebook.com/sharer.php?u=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://www.facebook.com/sharer.php?u=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToInstagram(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://www.instagram.com/?url=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://www.instagram.com/?url=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  shareToTwitter(key) {
    if (localStorage.getItem('storeName')) {
      this.storeName = localStorage.getItem('storeName');
    }
    if (this.storeName === undefined) {
      this.storeName = environment.storeId;
    }
    let data = { id: key, store_name: this.storeName };
    this.store.getProductById(data).subscribe((res) => {
      let product = res.message;
      this.router
        .navigate([])
        .then((result) => {
          this.meta.updateTag({
            name: 'description',
            content: product.description,
          });
          this.meta.updateTag({
            name: 'image',
            content: product.images1,
          });
          this.title.setTitle(product.name);
        })
        .then((res) => {
          let sName = localStorage.getItem('haveStoreName');

          if (sName === 'true') {
            window.open(
              'https://twitter.com/share?url=' +
                window.location.href +
                '/product/' +
                key,
              '_blank'
            );
            this.storeName = localStorage.getItem('storeName');
          } else {
            window.open(
              'https://twitter.com/share?url=' +
                window.location.href +
                'product/' +
                key,
              '_blank'
            );
          }
        });
    });
  }
  isReview = false;
  changeCategory(index: any) {
    this.isReview = false;
    if (index === 'reviews') {
      if (this.storeData && this.storeData.reviews) {
        if (this.common.sizeOfObj(this.storeData.reviews)) {
          this.selectedCategory = this.storeData.reviews;
          this.selectedCategoryProducts = [];
          for (const key in this.storeData.reviews) {
            this.selectedCategoryProducts.push(this.storeData.reviews[key]);
          }
          this.isReview = true;
        }
      } else {
        this.selectedCategoryProducts = [];
        this.isReview = false;
      }

      return;
    } else if (index === 'ALL') {
      if (this.storeData && this.storeData.sub_categories) {
        if (this.common.sizeOfObj(this.storeData.sub_categories)) {
          this.selectedCategory = 'ALL';
          this.selectedCategoryProducts = [];
          const activeCategories = this.common.getActiveCategories(
            this.storeData.sub_categories
          );

          for (const key in this.categoryProducts) {
            if (
              activeCategories.includes(
                this.categoryProducts[key].sub_category_id.toString()
              )
            ) {
              this.selectedCategoryProducts.push(this.categoryProducts[key]);
            }
          }
        }
      }
    } else {
      if (this.storeData && this.storeData.sub_categories) {
        if (this.common.sizeOfObj(this.storeData.sub_categories)) {
          this.selectedCategory = this.storeData.sub_categories[index];
          this.selectedCategoryProducts = [];

          for (const key in this.categoryProducts) {
            if (
              this.selectedCategory.id ===
              this.categoryProducts[key].sub_category_id
            ) {
              this.selectedCategoryProducts.push(this.categoryProducts[key]);
            }
          }
        }
      }
    }
  }
  imgValue: any;
  viewImg(img) {
    this.imgValue = img;
  }
  viewMainImg(images1) {
    this.imgValue = images1;
  }
  copyMessage(val: any) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = window.location.href;
    this.swalToastMessage(this.pageType + ' link copied!', 'success');

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  chunk(arr: any, chunkSize: any) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  checkStoreStatus(start, end) {
    var timeToCheckMilli = new Date().getTime();
    let startTimeMilli = moment(start, 'HH:mm A').valueOf();
    let endTimeMilli = moment(end, 'HH:mm A').valueOf();
    if (
      timeToCheckMilli >= startTimeMilli &&
      timeToCheckMilli <= endTimeMilli
    ) {
      this.storeOpenStatus = 'Open';
    } else {
      this.storeOpenStatus = 'Closed';
    }
  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }
  scrollToElement(): void {
    document.getElementById('target').scrollIntoView({ behavior: 'smooth' });
  }
  scrollToElementCD() {
    document.getElementById('cDetails').scrollIntoView({ behavior: 'smooth' });
  }

  total: number = 0;
  payable: number = 0;

  addcartProducts: any = [];

  addToCartCOF(product: any) {
    this.addcartProducts = product;
    this.addcartProducts.images = product.images1;
    this.addcartProducts.quantity = 1;
    this.addcartProducts.product_payable_price = this.productPayablePrice;

    if (this.addcartProducts.hasOwnProperty('has_options')) {
      if (this.addcartProducts.has_options.image) {
        this.singleProduct.images = this.singleProduct.has_options.image;
        this.singleProduct.images1 = this.singleProduct.has_options.image;
      } else {
        if (this.common.defaultImg) {
          this.singleProduct.images = this.common.defaultImg;
          this.singleProduct.images1 = this.common.defaultImg;
        }
      }
    } else {
      if (this.common.defaultImg) {
        this.singleProduct.images = this.common.defaultImg;
        this.singleProduct.images1 = this.common.defaultImg;
      }
    }

    this.storageService.setStorageValueCOF(
      this.addcartProducts,
      this.storeData.store.name.trim() + '-' + 'my-cart-cof'
    );
    this.swalToastMessage(product.name + ' added to cart!', 'success');
    setTimeout(() => {
      this.getCartItemsCOF();
    }, 800);
  }
  shareContent = {
    url: window.location.href,
  };
  getCartItemsCOF() {
    this.storageService
      .getStorageCOF(this.storeData.store.name.trim() + '-' + 'my-cart-cof')
      .then((products) => {
        if (products && products.length) {
          this.cartProducts = products;
          this.total = 0;
          for (var i = 0; i < this.cartProducts.length; i++) {
            this.total +=
              this.cartProducts[i].discount_price *
              this.cartProducts[i].quantity;
            this.loadingCart = false;
          }
        } else {
          this.cartProducts = [];
          this.loadingCart = false;
        }
      });
  }
  getCustomerDetailsCOF() {
    this.storageService
      .getStorageCOF('my-customer-cof')
      .then((details) => {
        if (details && details.length) {
          this.addCustomerDetails = details;
          if (this.addCustomerDetails[0].selectedDelivery.trim() === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        } else {
          this.addCustomerDetails = [];
        }
      })
      .catch((err) => (this.addCustomerDetails = []));
  }
  notifyMessage(themsg, ntype) {
    $('#notification').finish();
    $('#notification').html(themsg);
    $('#notification').removeClass();
    $('#notification').addClass('notification');
    $('#notification').addClass('is-' + ntype);
    $('#notification').fadeIn().delay(800).fadeOut('slow');
  }

  swalToastMessage(text, icon, timer = 3000) {
    const Toast = swal.mixin({
      toast: true,
      background: '#2f3542',
      position: 'bottom-start',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      },
    });
    Toast.fire({
      icon: icon,
      title: text,
      background: '#2f3542',
    });
  }

  generateOrder() {
    var mode = $('#selectedDelivery').val();

    if ($('#contactNo').val().trim() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      return false;
    }

    if ($('#name').val().trim() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val().trim() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        return false;
      }
      if ($('#address2').val().trim() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        return false;
      }
    }

    var loadUrl = $('#baseUrl').val() + $('#subDomain').val() + '/cart-order';
    var href = '';
    $.ajax({
      url: loadUrl,
      method: 'GET',
      async: false,
      context: this,
      data: {
        selected_delivery: $('#selectedDelivery').val(),
        name: $('#name').val(),
        contact_no: $('#contactNo').val(),
        address1: $('#address1').val(),
        address2: $('#address2').val(),
        pincode: '',
        notes: $('#notes').val(),
      },
      success: function (data) {
        data = JSON.parse(data);

        var orderContent = '';
        this.getCartItemsCOF();

        if (this.cartProducts && this.cartProducts.length) {
          var itemsList = '';
          for (var i = 0; i < this.cartProducts.length; i++) {
            itemsList =
              itemsList +
              '▪️' +
              this.cartProducts[i].name +
              ' x ' +
              this.cartProducts[i].quantity +
              ' x ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.cartProducts[i].discount_price +
              '  = ' +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              +(
                this.cartProducts[i].discount_price *
                this.cartProducts[i].quantity
              ) +
              '\n';
          }

          this.specialInstructions =
            `Special Instructions : ` + $('#notes').val() + `\n------\n`;

          if ($('#selectedDelivery').val() == 'd') {
            orderContent =
              `✅ Delivery Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity | Price | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details :\nName : ` +
              this.storeData.store.display_name +
              `\nDelivery Option : ` +
              this.delivery_options.name
                ? this.delivery_options.name
                : 'Default' +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
          } else {
            orderContent =
              `✅ Pickup Order No : #420\nfrom www.store.cartonfly.com/` +
              this.storeData.store.name +
              `\n------\n# | Item(s) | Quantity | Price | Total\n` +
              itemsList +
              `------\nNotes : We are open from 9:00 AM to 9:00 PM (monday to friday)\n------\nSubtotal : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              this.total.toFixed(2) +
              `\nTotal(Incl Tax) : ` +
              new CurrencySymbolPipe().transform(
                this.storeData.store.currency_symbol
              ) +
              ' ' +
              (this.total + this.total * (18 / 100)).toFixed(2) +
              `\n------\nStore Details For Pickup:\nName : ` +
              this.storeData.store.display_name +
              `\nContact : ` +
              this.storeData.store.whatsapp_number +
              `\nAddress : ` +
              this.storeData.store.address +
              `\n------\n` +
              this.specialInstructions +
              this.storeData.store.display_name +
              ` has received your order.\nNow proceed to payment by using Payment Options given below to complete the order.\n\n💳 Payment Options : 💸\n1. We accept cash on delivery\n2. Cash on pick up\n\n------\nCustomer Details :\nName : ` +
              $('#name').val() +
              `\nContact : ` +
              $('#contactNo').val() +
              `\n------\nHave a nice day, Thank you :)\n`;
          }
        }

        orderContent = encodeURI(orderContent);
        var isSafari =
          navigator.vendor &&
          navigator.vendor.indexOf('Apple') > -1 &&
          navigator.userAgent &&
          navigator.userAgent.indexOf('CriOS') == -1 &&
          navigator.userAgent.indexOf('FxiOS') == -1;
        if (isSafari) {
          href =
            'https://wa.me/' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        } else {
          href =
            'https://api.whatsapp.com/send?phone=' +
            $('#whatsapp').val() +
            '&text=' +
            orderContent.toString();
        }
      },
    }).done(function () {
      window.open(href, '_blank');
    });
  }

  addCustomerDetails: any = [];
  addQuantityCOF(product, index) {
    if (product.quantity) {
      product.quantity = product.quantity + 1;
    } else {
      product.quantity = 1;
      product.quantity = product.quantity + 1;
    }
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  goToCreateOrderOptions() {
    this.spinner.show();
    var mode = $('#selectedDelivery').val();

    if ($('#delivery-mode').val() === 'none') {
      this.swalToastMessage('Please choose order preference', 'warning');
      this.spinner.hide();
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if (this.storeData.delivery_options.length !== 0) {
        if ($('#delivery_option').val().trim() == 'none') {
          this.swalToastMessage('Please choose delivery option', 'warning');
          this.spinner.hide();
          return false;
        }
      }
    }

    if (this.storeData.bank_options.length !== 0) {
      if ($('#bank_option').val().trim() == 'none') {
        this.swalToastMessage(
          'Please choose preferred mode of payment',
          'warning'
        );
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactEmail').val().trim() == '') {
      this.swalToastMessage('Please enter your email address', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test($('#contactEmail').val().trim()) == false) {
        this.swalToastMessage('Please enter valid email address', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#contactNo').val().trim() == '') {
      this.swalToastMessage('Please enter your contact number', 'warning');
      this.spinner.hide();
      return false;
    } else {
      var phoneNum = $('#contactNo').val().trim().replace(/[^\d]/g, '');
      if (phoneNum.length > 6 && phoneNum.length < 12) {
      } else {
        this.swalToastMessage('Please enter valid contact number', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if ($('#name').val().trim() == '') {
      this.swalToastMessage('Please enter your name', 'warning');
      this.spinner.hide();
      return false;
    }

    if ($('#selectedDelivery').val() == 'd') {
      if ($('#address1').val().trim() == '') {
        this.swalToastMessage('Address 1 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }

      if ($('#address2').val().trim() == '') {
        this.swalToastMessage('Address 2 is required for delivery', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    if (
      this.storeData.pickup_days.length !== 0 &&
      $('#selectedDelivery').val() == 'p'
    ) {
      if ($('#pickup_day').val().trim() == 'none') {
        this.swalToastMessage('Please choose pickup day', 'warning');
        this.spinner.hide();
        return false;
      }
    }

    var custDetails = {
      selectedDelivery: $('#selectedDelivery').val().trim(),
      name: $('#name').val().trim(),
      contactNo: $('#contactNo').val().trim(),
      contactEmail: $('#contactEmail').val().trim(),
      address1: $('#address1').val().trim(),
      address2: $('#address2').val().trim(),
      pincode: '',
      notes: $('#notes').val().trim(),
    };

    this.storageService.clearStorageValueCOF('my-customer-cof').then((res) => {
      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }
      this.storageService.setCustomerStorageValueCOF(
        custDetails,
        'my-customer-cof'
      );
      this.addCustomerDetails[0] = custDetails;
    });

    let data = JSON.stringify({
      account_id: this.storeData.store.account_email_id,
      store_name: this.storeName,
      email: this.storeData.store.account_email_id,
      selected_delivery: $('#selectedDelivery').val(),
      name: $('#name').val(),
      contact_no: $('#contactNo').val(),
      contact_email: $('#contactEmail').val(),
      address1: $('#address1').val(),
      address2: $('#address2').val(),
      pincode: '',
      notes: $('#notes').val(),

      currency_symbol: this.storeData.store.currency_symbol,
      total: this.total,
      delivery_charges:
        $('#selectedDelivery').val() === 'd'
          ? this.storeData.store.delivery_charges
          : 0, //new
      delivery_options: this.chosedDeliveryOption
        ? this.chosedDeliveryOption
        : 'false', //new
      bank_options: this.storeData.bank_options[$('#bank_option').val()]
        ? this.storeData.bank_options[$('#bank_option').val()]
        : 'N/A', //new

      pickup_day:
        $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
          ? $('#pickup_day').val()
          : 'N/A', //new

      is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
      coupon_discount_value: this.appliedCouponDetails
        ? (
            (this.total * this.appliedCouponDetails.discount_percentage) /
            100
          ).toFixed(2)
        : 0, //new
      coupon_details: this.appliedCouponDetails
        ? this.appliedCouponDetails
        : 'false', //new
      payable:
        $('#selectedDelivery').val() === 'd'
          ? (
              this.total +
              (this.storeData.store.delivery_charges
                ? this.storeData.store.delivery_charges
                : 0) -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2)
          : (
              this.total -
              (this.appliedCouponDetails
                ? (this.total * this.appliedCouponDetails.discount_percentage) /
                  100
                : 0)
            ).toFixed(2), //new
      items: this.cartProducts,
    });

    this.spinner.show();
    this.store.placeOrder(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          let data = {
            mode: 'WP',
          };
          if (data.mode === 'WP') {
            var orderContent = '';
            this.getCartItemsCOF();

            if (this.cartProducts && this.cartProducts.length) {
              var itemsList = '';
              for (var i = 0; i < this.cartProducts.length; i++) {
                itemsList =
                  itemsList +
                  '▪️' +
                  this.cartProducts[i].name +
                  ' x ' +
                  this.cartProducts[i].quantity +
                  ' x ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.cartProducts[i].discount_price +
                  '  = ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.cartProducts[i].discount_price *
                    this.cartProducts[i].quantity +
                  '\n';
              }

              this.specialInstructions =
                `Special Instructions : ` + $('#notes').val() + `\n------\n`;

              if ($('#selectedDelivery').val() == 'd') {
                orderContent =
                  `✅ Delivery Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Price | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nDelivery Charges : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.storeData.store.delivery_charges
                    ? +this.storeData.store.delivery_charges
                    : 0
                  ).toFixed(2) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        (this.storeData.store.delivery_charges
                          ? +this.storeData.store.delivery_charges
                          : 0) -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nDelivery Partner : \n` +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.name
                    : 'Default') +
                  ' ' +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  (this.chosedDeliveryOption
                    ? this.chosedDeliveryOption.price
                    : 0) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nStore Details :\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details For Delivery :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\nAddress : ` +
                  $('#address1').val() +
                  `,` +
                  $('#address2').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              } else {
                orderContent =
                  `✅ Pickup Order No : #__ORDER__ID__\nfrom www.store.cartonfly.com/` +
                  this.storeData.store.name +
                  `\n------\n# | Item(s) | Quantity | Price | Total\n` +
                  itemsList +
                  `\n------\nSubtotal : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  this.total.toFixed(2) +
                  `\n------\nDiscount : -` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  (this.appliedCouponDetails
                    ? (
                        (this.total *
                          this.appliedCouponDetails.discount_percentage) /
                        100
                      ).toFixed(2)
                    : (0).toFixed(2)) +
                  `\n------\nPayable : ` +
                  new CurrencySymbolPipe().transform(
                    this.storeData.store.currency_symbol
                  ) +
                  ' ' +
                  ($('#selectedDelivery').val() === 'd'
                    ? (
                        this.total +
                        this.storeData.store.delivery_charges -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)
                    : (
                        this.total -
                        (this.appliedCouponDetails
                          ? (this.total *
                              this.appliedCouponDetails.discount_percentage) /
                            100
                          : 0)
                      ).toFixed(2)) +
                  `\n------\nPreferred mode of payment : \n` +
                  (this.storeData.bank_options[$('#bank_option').val()]
                    ? this.storeData.bank_options[$('#bank_option').val()]
                        .name +
                      `\n` +
                      this.storeData.bank_options[$('#bank_option').val()]
                        .bank_details
                    : 'N/A') +
                  `\n------\nPreferred pickup day : \n` +
                  ($('#pickup_day').val() && $('#pickup_day').val() !== 'none'
                    ? $('#pickup_day').val()
                    : 'N/A') +
                  `\n------\nStore Details For Pickup:\nName : ` +
                  this.storeData.store.display_name +
                  `\nContact : ` +
                  this.storeData.store.whatsapp_number +
                  `\nAddress : ` +
                  this.storeData.store.address +
                  `\n------\n` +
                  this.specialInstructions +
                  this.storeData.store.display_name +
                  ` has received your order.\n------\nCustomer Details :\nName : ` +
                  $('#name').val() +
                  `\nEmail : ` +
                  $('#contactEmail').val() +
                  `\nContact : ` +
                  $('#contactNo').val() +
                  `\n------\nHave a nice day, Thank you :)\n`;
              }

              this.orderContent = orderContent;

              this.orderContent = this.orderContent.replace(
                '__ORDER__ID__',
                res.order_id
              );
            }

            orderContent = encodeURI(orderContent);
            var isSafari =
              navigator.vendor &&
              navigator.vendor.indexOf('Apple') > -1 &&
              navigator.userAgent &&
              navigator.userAgent.indexOf('CriOS') == -1 &&
              navigator.userAgent.indexOf('FxiOS') == -1;
            if (isSafari) {
              this.href =
                'https://wa.me/' +
                $('#whatsapp').val() +
                '?text=' +
                orderContent.toString();
            } else {
              this.href =
                'https://api.whatsapp.com/send?phone=' +
                $('#whatsapp').val() +
                '&text=' +
                orderContent.toString();
            }
          } else if (data.mode == 'SMS') {
            this.href =
              'sms://' +
              $('#whatsapp').val() +
              ';?&body=' +
              orderContent.toString();
          }

          setTimeout(() => {
            this.getCartItemsCOF();
            if (!this.ref['destroyed']) {
              this.ref.detectChanges();
            }
            this.openOrderDetails();
            this.spinner.hide();
          }, 1000);
        } else {
          this.swalToastMessage(
            'Unable to create order, try again later!',
            'warning'
          );
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage(
          'Unable to create order, try again later!',
          'warning'
        );
      }
    );
  }
  async openOrderDetails() {
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    let modal = await this.modalController.create({
      component: CreateOrderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        cartArr: this.cartProducts,
        storeDetailsArr: this.storeData.store,
        customerDetailsArr: this.addCustomerDetails,
        totalAmount: this.total,
        waHref: this.href,
        orderContent: this.orderContent,
        accountEmail: this.storeData.store.account_email_id,
        storeName: this.storeName,
        delivery_charges:
          $('#selectedDelivery').val() === 'd'
            ? this.storeData.store.delivery_charges
            : 0, //new
        delivery_options: this.chosedDeliveryOption
          ? this.chosedDeliveryOption
          : 'false', //new
        bank_options: this.storeData.bank_options[$('#bank_option').val()]
          ? this.storeData.bank_options[$('#bank_option').val()]
          : 'N/A', //new
        pickup_day:
          $('#pickup_day').val() && $('#pickup_day').val() !== 'none'
            ? $('#pickup_day').val()
            : 'N/A', //new
        is_coupon_applied: this.appliedCouponDetails ? 'true' : 'false', //new
        coupon_discount_value: this.appliedCouponDetails
          ? (
              (this.total * this.appliedCouponDetails.discount_percentage) /
              100
            ).toFixed(2)
          : 0, //new
        coupon_details: this.appliedCouponDetails
          ? this.appliedCouponDetails
          : 'false', //new
        payable:
          $('#selectedDelivery').val() === 'd'
            ? (
                this.total +
                this.storeData.store.delivery_charges -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2)
            : (
                this.total -
                (this.appliedCouponDetails
                  ? (this.total *
                      this.appliedCouponDetails.discount_percentage) /
                    100
                  : 0)
              ).toFixed(2), //new
        items: this.cartProducts,
      },
    });

    this.storageService
      .clearStorageValueCOF(
        this.storeData.store.name.trim() + '-' + 'my-cart-cof'
      )
      .then((res) => {
        this.swalToastMessage(
          'Your order taken successfully, You will receive call from the ' +
            this.storeData.store.display_name +
            ' team shortly.',
          'success',
          6000
        );
        this.getCartItemsCOF();
        this.appliedCouponDetails = false;
        this.couponCode = null;

        if (this.addCustomerDetails) {
          if (this.addCustomerDetails[0].selectedDelivery.trim() === 'd') {
            $('.show-address').show();
            $('.show-pickup-total').hide();
            $('#selectedDelivery').val('d');
            $('#delivery-mode').val('d');
            $('#pickup').removeClass('active');
            $('#delivery').addClass('active');
          } else {
            $('.show-address').hide();
            $('.show-pickup-total').show();
            $('#selectedDelivery').val('p');
            $('#delivery-mode').val('p');
            $('#pickup').addClass('active');
            $('#delivery').removeClass('active');
          }
        }

        if (this.storeData.store) {
          if (this.storeData.store.order_mode) {
            if (
              this.storeData.store.order_mode === 'all' ||
              this.storeData.store.order_mode === 'delivery'
            ) {
              $('.show-address').show();
              $('.show-pickup-total').hide();
              $('#selectedDelivery').val('d');
              $('#delivery-mode').val('d');
              $('#pickup').removeClass('active');
              $('#delivery').addClass('active');
            } else {
              $('.show-address').hide();
              $('.show-pickup-total').show();
              $('#selectedDelivery').val('p');
              $('#delivery-mode').val('p');
              $('#pickup').addClass('active');
              $('#delivery').removeClass('active');
            }
          }
        }
      });

    modal.dismiss();
    return await modal.present();
  }

  viewImageSlider(image, additionalImagesArr) {
    if (additionalImagesArr) {
      additionalImagesArr['main'] = { image: image };
    } else {
      additionalImagesArr = { main: { image: image } };
    }

    this.openImageSlider(additionalImagesArr);
    return;
  }
  async openImageSlider(additionalImagesArr) {
    this.spinner.show();
    if (!this.ref['destroyed']) {
      this.ref.detectChanges();
    }

    this.spinner.hide();

    let modal = await this.modalController.create({
      component: ImageSliderComponent,
      cssClass: 'fullscreen',
      componentProps: {
        additionalImagesArr: additionalImagesArr,
      },
    });

    modal.dismiss();
    return await modal.present();
  }
}
