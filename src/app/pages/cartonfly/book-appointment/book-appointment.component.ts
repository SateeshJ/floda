
import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StorageService } from '../../../services/storage.service';
import * as copy from 'copy-to-clipboard';
import { ToastrService } from 'ngx-toastr';
import * as $ from "jquery"
import swal from 'sweetalert2';
import {
  CurrencySymbolPipe
} from '../../../currency-symbol.pipe';
import {
  StoreService
} from '../../../services/store.service';
import {
  NgxSpinnerService
} from 'ngx-spinner';
@Component({
  selector: 'app-cart',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.scss'],
})
export class BookAppointmentComponent implements OnInit {

  orderContent: any;

  @Input() public customerDetailsArr: any[];
  @Input() public storeDetailsArr: any;
  @Input() public appointmentDetailsArr: any;
  constructor(
    private store: StoreService,
    public modalController: ModalController,
    public storageService: StorageService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit() {
    this.orderContent = false;
  }




  generateOrder() {
    window.open("https://api.whatsapp.com/send?phone=" + $('#whatsapp').val() + "&text=" + encodeURIComponent(this.orderContent.toString()), '_blank');
  }

  copy_link() {
    copy(this.orderContent);
    this.toastr.success("Appointment request content copied!");
  }

  swalToastMessage(text, icon, timer = 3000) {
    const Toast = swal.mixin({
      toast: true,
      background: '#2f3542',
      position: 'bottom-start',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer)
        toast.addEventListener('mouseleave', swal.resumeTimer)
      }
    })
    Toast.fire({
      icon: icon,
      title: text,
      background: '#2f3542'
    })

  }
  submitForm() {
    this.spinner.show();
    if ($('#attender_name').val().trim() == '') {
      this.swalToastMessage("Please enter your name", "warning");
      this.spinner.hide();
      return false;
    }

    if ($('#attender_contactNo').val().trim() == '') {
      this.swalToastMessage("Please enter your contact number", "warning");
      this.spinner.hide();
      return false;
    }
    if (this.appointmentDetailsArr.length !== 0) {
      if ($('#appointment_option').val().trim() == 'none') {
        this.swalToastMessage("Please choose preferred option", "warning");
        this.spinner.hide();
        return false;
      }
    }
    if ($('#appointment_date_time').val().trim() == '') {
      this.swalToastMessage("Please choose your date & time", "warning");
      this.spinner.hide();
      return false;
    }



    var appointmentDetails = {
      "name": $('#attender_name').val().trim(),
      "contactNo": $('#attender_contactNo').val().trim(),
      "appointmentOption": $('#appointment_option').val().trim(),
      "appointmentDateAndTime": $('#appointment_date_time').val().trim(),
    };


    let data = JSON.stringify({
      account_id: this.storeDetailsArr.account_email_id,
      store_name: this.storeDetailsArr.name,
      email: this.storeDetailsArr.account_email_id,
      name: $('#attender_name').val(),
      contact_no: $('#attender_contactNo').val(),
      appointmentOption: (this.appointmentDetailsArr[$('#appointment_option').val()]) ? this.appointmentDetailsArr[$('#appointment_option').val()] : 'N/A',//new 
      appointmentDateAndTime: $('#appointment_date_time').val().trim(),
      currency_symbol: this.storeDetailsArr.currency_symbol,
    })




    this.spinner.show();
    this.store.bookAnAppointment(data).subscribe((res: any) => {
      this.spinner.hide();
      if (res.status === "success") {
        this.swalToastMessage(res.message, "success");


        this.orderContent =
          `
Appointment Request -
        
Appointment ID : `+ res.appointment_id + `
Appointment Date : `+ res.appointment_date + `
           
Customer : `+ $('#attender_name').val() + `
Mobile : `+ $('#attender_contactNo').val() + ` 
        
Preferred Option : `+ this.appointmentDetailsArr[$('#appointment_option').val()].name + ` ` + new CurrencySymbolPipe().transform(this.storeDetailsArr.currency_symbol) + this.appointmentDetailsArr[$('#appointment_option').val()].price + `
        
Have a nice day, Thank you :)
`;


      } else {
        this.swalToastMessage("Unable to book an  appointment, try again later!", "warning");
      }
    },
      (err: any) => {
        this.spinner.hide();
        this.swalToastMessage("Unable to book an  appointment, try again later!", "warning");
      })
  }
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
