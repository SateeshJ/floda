import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from 'src/app/services/store.service';
import * as $ from 'jquery';
import { ModalController } from '@ionic/angular';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions.component';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  constructor(
    private store: StoreService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute
  ) {
    $(document).ready(function () {
      $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $('.navbar-collapse').hasClass('show');
        if (_opened === true && !clickover.hasClass('navbar-toggler')) {
          $('.navbar-toggler').click();
        }
      });
    });
  }
  loading = true;
  posters = [
    { img: 'assets/posters/2.png' },
    { img: 'assets/posters/13.png' },
    { img: 'assets/posters/4.png' },
    { img: 'assets/posters/3.png' },
    { img: 'assets/posters/20.jpg' },
    { img: 'assets/posters/24.jpg' },
    { img: 'assets/posters/26.png' },
    { img: 'assets/posters/19.png' },
    { img: 'assets/posters/28.jpg' },
    { img: 'assets/posters/27.png' },
    { img: 'assets/posters/15.jpg' },
    { img: 'assets/posters/18.png' },
  ];
  ngOnInit() {
    let data = {
      email: 'demogroceries@gmail.com',
      secret: '8202991320697607',
    };
    this.store.getAllStoreDetailsListForHomePage(data).subscribe((res) => {
      this.allStores = res.message;
      this.filteredStores = this.allStores.filter((item) => {
        return item.logo.match(/\.(jpeg|jpg|gif|png)$/) != null;
      });
      this.loading = false;
    });
  }
  searchStoreName: any;
  filteredStores: any = [];
  allStores: any = [];
  storeData: any = []
  searchStore(a: any, b: any) {
    this.filteredStores = this.filterStoreItems(this.searchStoreName);
    this.scrollToTop();
  }
  filterStoreItems(searchTerm: any) {
    return this.allStores.filter((item) => {
      return (
        item.store_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      );
    });
  }
  isValidImg: any;
  update(event) {
    this.isValidImg = event.type;
  }
  getContent() {
    return document.querySelector('ion-content');
  }

  scrollToTop() {
    this.getContent().scrollToTop(500);
  }
  gotoAdminPage() {
    window.open('http://admin.cartonfly.com/', '_target');
  }
  storeName: any;
  getStoreDetails() {
    this.activatedRoute.params.subscribe((res) => {
      this.storeName = res.storeId;
 
      if (this.storeName === "" || this.storeName === undefined) {
        if (environment.isWhiteLabeled) {
          this.storeName = environment.storeId;
        } else {
          this.router.navigate(["/"]);
        }
      }
      let data = {
        store_name: this.storeName,
      };
      this.spinner.show();
      this.store.fetchStoreDetails(data).subscribe(
        (res: any) => {
          if (!res.message) {
            this.spinner.hide();
            this.storeData = [];
            return true;
          }
        },
        (err: any) => {
          this.spinner.hide();
          this.storeData = [];
        }
      );
    });
  }
  async openTerms(event) {
    this.spinner.show();
    const modal = await this.modalController.create({
      component: TermsAndConditionsComponent,
      cssClass: "terms-and-conditions",
      componentProps: {
        storeName: this.storeName,
        title: event,
      },
    });
    return await modal.present();
  }
}
